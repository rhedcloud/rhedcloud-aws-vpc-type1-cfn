# rhedcloud-aws-vpc-type1-cfn

RHEDcloud AWS CloudFormation Template for a type 1 VPC: Extension of enterprise network to AWS VPC with all access ingress and egressing through enterprise network.

## Parameters

* **DeployInferenceEc2Endpoint**: Conditional Parameter: Yes - will provision EC2 Inference Endpoint in the VPC; No - will not provision endpoint.
* **ManagementSubnet1Cidr**: Central IT only subnet in availability zone A used for management infrastructure.
* **ManagementSubnet2Cidr**: Central IT only subnet in availability zone B used for management infrastructure.
* **PrivateSubnet1Cidr**: Private subnet in availability zone A.
* **PrivateSubnet2Cidr**: Private subnet in availability zone B.
* **PublicSubnet1Cidr**: Public subnet in availability zone A.
* **PublicSubnet2Cidr**: Public subnet in availability zone B.
* **RHEDcloud1CustomerGatewayIp**: IP Address of enterprise VPN device 1
* **RHEDcloud2CustomerGatewayIp**: IP Address of enterprise VPN device 2
* **RHEDcloudVpn1InsideTunnelCidr1**: IP Address range for RHEDcloud VPN 1 Tunnel 1
* **RHEDcloudVpn1InsideTunnelCidr2**: IP Address range for RHEDcloud VPN 1 Tunnel 2
* **RHEDcloudVpn2InsideTunnelCidr1**: IP Address range for RHEDcloud VPN 2 Tunnel 1
* **RHEDcloudVpn2InsideTunnelCidr2**: IP Address range for RHEDcloud VPN 2 Tunnel 2
* **TransitGatewayId**: The Id of the transit gateway to attach the VPC.
* **VpcCidr**: The IP CIDR range allocated for the VPC.
* **VpcConnectionMethod**: Conditional Parameter: VPN - will provision VPN connections and attach them to the VGW; TGW - will provision the TGW attachment.

## Resources

* **ElasticInferenceRuntimeEndpoint**: The Elastic Inference Runtime endpoint makes it possible to route directly from infrastructure inside the VPC (like ec2 instances) to the Elastic Inference accelerato service.  Without this endpoint, customers will not be able to use Elastic Inference accelerators.
* **ElasticInferenceSecurityGroup**: The ElasticInferenceSecurityGroup security group is created for the Elastic Inference Runtime VPC Endpoint.
* **InboundVpcToVpcAllowAllLocalNetworkAclEntry**: This NACL entry allows all inbound traffic.
* **InboundVpcToVpcAllowAllNetworkAclEntry**: This NACL entry allows all inbound traffic.
* **InboundVpcToVPcDenyNetworkAclEntry**: This NACL entry denies inbound access to the VPC from the summarized CIDR range of the entire RHEDcloud environment.  This needs to be parameterized.
* **ManagementRoute**: The ManagementRoute is the default route (0.0.0.0/0) for the two management subnets created in this template.  This route is installed in the ManagementRouteTable.
* **ManagementRouteTable**: The MnagementRouteTable is the route table for the management subnets.  This route table is associated with the VPC created in this template, has three routes (local, default, and S3 endpoint), and has the two management subnets associated to it.
* **ManagementSubnet1**: The ManagementSubnet1 subnet is a network zone created in availability zone A within the VPC for Central IT to use for infrastructure used in support of the VPC, like monitoring or testing instances.  At present time, IT doesn't plan to launch any infrastructure in the management subnets of a Type 1 VPC.
* **ManagementSubnet1RouteTableAssociation**: The ManagementSubnet1RouteTableAssociation associates ManagementSubnet1 with the ManagementRouteTable.
* **ManagementSubnet2**: The ManagementSubnet2 subnet is a network zone created in availability zone B within the VPC for central IT to use for infrastructure used in support of the VPC, like monitoring or testing instances.  At present time, IT doesn't plan to launch any infrastructure in the management subnets of a Type 1 VPC.
* **ManagementSubnet2RouteTableAssociation**: The ManagementSubnet2RouteTableAssociation associates ManagementSubnet2 with the ManagementRouteTable.
* **MgmtSubnet1NetworkAclAssociation**: The MgmtSubnet1NetworkAclAssociation associates the VpcToVpcNetworkAcl to the ManagementSubnet1 subnet, which means instances deployed within the ManagementSubnet1 will be subject to this NACL.
* **MgmtSubnet2NetworkAclAssociation**: The MgmtSubnet2NetworkAclAssociation associates the VpcToVpcNetworkAcl to the ManagementSubnet2 subnet, which means instances deployed within the MgmtSubnet2 will be subject to this NACL.
* **OutboundVpcToVpcAllowAllNetworkAclEntry**: This NACL entry allows all outbound traffic.
* **PrivateRoute**: The PrivateRoute is the default route (0.0.0.0/0) for the two private subnets created in this template.  This route is installed in the PrivateRouteTable.
* **PrivateRouteTable**: The PrivateRouteTable is the route table for the private subnets.  This route table is associated with the VPC created in this template, has three routes (local, default, and S3 endpoint), and has the two private subnets associated to it.
* **PrivateSubnet1**: The PrivateSubnet1 subnet is a network zone created in availability zone A within the VPC for customers to deploy non-internet-facing components of applications such as databases and compute clusters.
* **PrivateSubnet1NetworkAclAssociation**: The PrivateSubnet1NetworkAclAssociation associates the VpcToVpcNetworkAcl to the PrivateSubnet1 subnet, which means instances deployed within the PrivateSubnet1 will be subject to this NACL.
* **PrivateSubnet1RouteTableAssociation**: The PrivateSubnet1RouteTableAssociation associates PrivateSubnet1 with the PrivateRouteTable.
* **PrivateSubnet2**: The PrivateSubnet2 subnet is a network zone created in availability zone B within the VPC for customers to deploy non-internet-facing components of applications such as databases and compute clusters.
* **PrivateSubnet2NetworkAclAssociation**: The PrivateSubnet2NetworkAclAssociation associates the VpcToVpcNetworkAcl to the PrivateSubnet2 subnet, which means instances deployed within the PrivateSubnet2 will be subject to this NACL.
* **PrivateSubnet2RouteTableAssociation**: The PrivateSubnet2RouteTableAssociation associates PrivateSubnet2 with the PrivateRouteTable.
* **PublicRoute**: The PublicRoute is the default route (0.0.0.0/0) for the two public subnets created in this template.  This route is installed in the PublicRouteTable.
* **PublicRouteTable**: The PublicRouteTable is the route table for the public subnets.  This route table is associated with the VPC created in this template, has three routes (local, default, and S3 endpoint), and has the two public subnets associated to it.
* **PublicSubnet1**: The PublicSubnet1 subnet is a network zone created in availability zone A within the VPC for customers to deploy public facing infrastructure, like web servers.  While this is not truly a public subnet, because all access to the VPC is through the RHEDcloud network and existing RHEDcloud network controls RHEDcloud retains that terminology to align with AWS documentation.
* **PublicSubnet1NetworkAclAssociation**: The PublicSubnet1NetworkAclAssociation associates the VpcToVpcNetworkAcl to the PublicSubnet1 subnet, which means instances deployed within the PublicSubnet1 will be subject to this NACL.
* **PublicSubnet1RouteTableAssociation**: The PublicSubnet1RouteTableAssociation associates PublicSubnet1 with the PublicRouteTable.
* **PublicSubnet2**: The PublicSubnet2 subnet is a network zone created in availability zone B within the VPC for customers to deploy public facing infrastructure, like web servers.  While this is not truly a public subnet, because all access to the VPC is through the RHEDcloud network and existing RHEDcloud network controls RHEDcloud retains that terminology to align with AWS documentation.
* **PublicSubnet2NetworkAclAssociation**: The PublicSubnet2NetworkAclAssociation associates the VpcToVpcNetworkAcl to the PublicSubnet2 subnet, which means instances deployed within the PublicSubnet2 will be subject to this NACL.
* **PublicSubnet2RouteTableAssociation**: The PublicSubnet2RouteTableAssociation associates PublicSubnet2 with the PublicRouteTable.
* **RHEDcloud1CustomerGateway**: The Customer Gateway is required for the VPN setup.  It defines the Internet-routable IP address of the on-premise, enterprise router that will serve as VPN endpoint 1.
* **RHEDcloud2CustomerGateway**: The Customer Gateway is required for the VPN setup.  It defines the Internet-routable IP address of the on-premise, enterprise router that will serve as VPN endpoint 2.
* **RHEDcloudManagementSubnetPolicy**: The ManagementSubnetPolicy is intended to prevent customer administrator role from deploying infrastructure in the management subnets. This policy prevents most operations on the two management subnets by customer administrator role.
* **RHEDcloudVpnConnection1**: The VPN connection sets up the VPN tunnel between this VPC's VGW and the RHEDcloud CGW1 - AWS Research VPC VPN Endpoint 1.
* **RHEDcloudVpnConnection2**: The VPN connection sets up the VPN tunnel between this VPC's VGW and the RHEDcloud CGW2 - AWS Research VPC VPN Endpoint 2.
* **S3Endpoint**: The s3 endpoint provides a direct network connection from the AWS s3 service to this VPC. The s3 endpoint makes it possible to route directly from infrastructure inside the VPC (like ec2 instances) to the s3 service.  Without this endpoint, traffic from infrastructure inside the VPC to AWS s3 would route back to RHEDcloud and through the Internet.
* **TgwManagementRoute**: The TgwManagementRoute is the default route (0.0.0.0/0) for the two management subnets created in this template.  This route is installed in the ManagementRouteTable.
* **TgwPrivateRoute**: The TgwPrivateRoute is the default route (0.0.0.0/0) for the two private subnets created in this template.  This route is installed in the PrivateRouteTable.
* **TgwPublicRoute**: The TgwPublicRoute is the default route (0.0.0.0/0) for the two public subnets created in this template.  This route is installed in the PublicRouteTable.
* **TransitGatewayAttachment**: The TGW attachment attaches the TGW passed in as a parameter to the VPC.
* **VgwAttachment**: The VGW attachment attaches the VGW called VirtualPrivateGateway to the VPC.
* **VirtualPrivateGateway**: The VirtualPrivateGateway (VGW) is the connection point for VPN Connected virtual interfaces and provides access to and from the networks inside the VPC. The VirtualPrivateGateway provides access to the networks inside the VPC by attaching to VPNs and becoming the default route destination for all of the subnets in a Type 1 VPC.
* **Vpc**: A Virutal Private Cloud (VPC) is an AWS networking construct used to create a logically isolated section of the AWS cloud.  This cloudformation template delivers a pre-configured RHEDcloud Type 1 VPC with security safeguards ready for customer use. The VPC is assigned an RHEDcloud CIDR space and several pre-defined tags.  From the CIDR space, this cloudformation template goes on to create the following which are all elements of the VPC: subnets, routes, route tables, enpoints, and VPC flow logs.
* **VpcFlowLog**: VPC Flow Logging puts network traffic information from the VPC into AWS logs. Flow logs are often used to troubleshoot connectivity and security issues.  This configuration enables VPC flow logging for all traffic in the VPC.
* **VpcLogGroup**: This log group provides a CloudWatch resource where the VPC Flow Logs will be stored.
* **VpcToVpcNetworkAcl**: This NACL prevents network traffic from one VPC from routing through RHEDcloud's border network into a second VPC.  This NACL protects against a compromised instance in one VPC from further compromising other VPCs.  This NACL is required by Enterprise Security.  Future network designs may place an RHEDcloud firewall between RHEDcloud VPCs, which would alleviate the need for this NACL.

## Pipeline Notes
Since the template is deployable using VPN connections or a TGW attachment, the pipeline has been restructure to test both deployments.  First, the pipeline deploys the Type1 VPC with VPN connections and then runs a series of tests against the deployed VPC.  The vast majority of these tests are common tests which will run with either a VPN or TGW deployment, but there are a few tests that will only run against the VPN deployment.  These tests have been marked with a PyTest Marker of "vpn_test".  If the VPN deployment is successful and all of the tests pass, the pipeline will delete the VPC that was deployed with VPN connection, and create a new VPC that is TGW attached.  Once the new VPC is deployed, the large set of common tests will be run, as well as, a select set of TGW only tests that are indicated with a PyTest marker of "tgw_test".  If the TGW deployment is successful and all of the tests pass, the pipeline will upload the new template to the DEV master account.
