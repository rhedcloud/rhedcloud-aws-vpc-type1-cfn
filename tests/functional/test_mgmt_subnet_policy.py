"""
=======================
test_mgmt_subnet_policy.py
=======================

"Type": "functional",
"Name": "test_mgmt_subnet_policy",
"Description": "This test performs a series of positive and negative checks to verify the RHEDcloudAdministratorRole role can not perform ec2 operations in either management subnet, but are allowed to perform ec2 operations in non-management subnets.",
"Plan": "Create a test user, attach user to the ManagementSubnetPolicy, and then perform the following tests that should be allowed: P1. Create ec2 instances in the following subnets: PublicSubnet1, PublicSubnet2, PrivateSubnet1, and PrivateSubnet2; and P2. Terminate the ec2 instances that were created in P1. Then perform the following negative test on each of the management subnets: N1. Attempt to create ec2 instances in each management subnet (ManagementSubnet1 and ManagementSubnet2).",
"ExpectedResult": "Success"

This test performs a series of positive and negative checks to verify the
RHEDcloudAdministratorRole role can not perform EC2 operations in either management
subnet, but are allowed to perform EC2 operations in non-management subnets.

* Create a test user
* Attach user to the ManagementSubnetPolicy
* Perform the following tests that should be allowed:

    P1. Create EC2 instances in the following subnets:

        * PublicSubnet1
        * PublicSubnet2
        * PrivateSubnet1
        * PrivateSubnet2

    P2. Terminate the EC2 instances that were created in P1.

* Perform the following negative test on each of the management subnets:

    N1. Attempt to create EC2 instances in each management subnet

        * ManagementSubnet1
        * ManagementSubnet2

${testcount:6}

"""

import time

import pytest

from aws_test_functions import (
    ClientError,
    attach_policy,
    create_session_for_test_user,
    find_policy_for_stack,
    get_subnet_id,
    get_stack_name_from_vpc_id,
    new_ec2_instance,
    new_test_user,
)


# subnets within each Type 1 VPC where EC2 instance creation should be
# permitted
positive_envs = (
    'Public Subnet 1',
    'Public Subnet 2',
    'Private Subnet 1',
    'Private Subnet 2',
)

# subnets within each Type 1 VPC where EC2 instance creation should NOT be
# permitted
negative_envs = (
    'MGMT1 - IT ONLY',
    'MGMT2 - IT ONLY',
)


@pytest.fixture
def subnet_id(vpc_id, env):
    return get_subnet_id(vpc_id, env)


@pytest.fixture
def user_session(vpc_id):
    with new_test_user('test_mgmt_subnet_policy') as u:
        stack_name = get_stack_name_from_vpc_id(vpc_id)
        # find_policy_for_stack function was changed to include /rhedcloud/ prefix
        arn = find_policy_for_stack(stack_name, 'RHEDcloudManagementSubnetPolicy')

        print('Testing stack: {}'.format(stack_name))
        print('Testing policy: {}'.format(arn))
        with attach_policy(u, 'AmazonEC2FullAccess', arn):
            session = create_session_for_test_user(u.user_name)

            # give a bit of time for changes to take effect
            time.sleep(1)

            yield session


@pytest.mark.parametrize("env", positive_envs)
@pytest.mark.slowtest
def test_ec2_instance_creation_allowed(vpc_id, subnet_id, user_session):
    """Positive tests"""

    print('Positive test: vpc_id={} subnet_id={}'.format(vpc_id, subnet_id))
    with new_ec2_instance('test_mgmt_subnet_policy', session=user_session, SubnetId=subnet_id):
        # allow the EC2 instance to register
        time.sleep(5)

        assert True, 'did not have permission to create EC2 instance in public or private subnet'


@pytest.mark.parametrize("env", negative_envs)
@pytest.mark.slowtest
def test_ec2_instance_creation_denied(vpc_id, subnet_id, user_session):
    """Negative tests"""

    print('Negative test: vpc_id={} subnet_id={}'.format(vpc_id, subnet_id))
    with pytest.raises(ClientError) as ex:
        with new_ec2_instance('test_mgmt_subnet_policy', session=user_session, SubnetId=subnet_id):
            # allow the EC2 instance to register
            time.sleep(5)

            assert False, 'had permission to create EC2 instance in MGMT subnet'

    assert 'UnauthorizedOperation' in str(ex), 'unexpected exception: {}'.format(ex)
