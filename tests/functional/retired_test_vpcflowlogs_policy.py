'''
-----------------
test_vpcflowlogs_policy.py
-----------------

"Type": "functional",
"Name": "test_vpcflowlogs_policy",
"Description": "This test performs a series of positive and negative checks to verify the RHEDcloudAdministratorRole can not modify or delete the VPC flow log role, settings, or events, but are allowed to view the logs.",
"Plan": "Create a test user, attach the RHEDcloudVpcFlowLogsPolicy policy to the user, and then perform the following tests that should be allowed: P1. User cannot add/modify the role policy P2. User cannot Create a log group. P3. User cannot delete a log group. P4. User cannot filter log events. P5. User cannot add/modify log events.",
"ExpectedResult": "Success"

${testcount:5}

'''

import time

import boto3
import pytest

from aws_test_functions import (
    ClientError,
    get_stack_name_from_vpc_id,
    find_policy_for_stack,
    create_session_for_test_user,
    new_test_user,
    attach_policy,
)


@pytest.fixture()
def session(vpc_id):
    """Create a new test user, attach the RHEDcloudAdministratorRolePolicy role
    policy along with the VPC-specific RHEDcloudVpcFlowLogsPolicy, and yield a
    session for the new test user.

    The test user is automatically cleaned up when the tests finish.

    """

    with new_test_user('test_vpcflowlogs_policy') as u:
        stack_name = get_stack_name_from_vpc_id(vpc_id)
        arn = find_policy_for_stack(stack_name, 'RHEDcloudVpcFlowLogsPolicy')

        print('Testing stack: {}'.format(stack_name))
        print('Testing policy: {}'.format(arn))
        with attach_policy(u, 'rhedcloud/RHEDcloudAdministratorRolePolicy', arn):
            session = create_session_for_test_user(u.user_name)

            # give a bit of time for changes to take effect
            time.sleep(1)

            yield session


@pytest.fixture()
def user_logs(session):
    """Create a new user session client for CloudWatch logs. This client is
    VPC-specific.

    """

    return session.client('logs')


def test_put_role_policy(vpc_id, session):
    vpc_flow_log_role_name = 'RHEDcloudVpcFlowLogRole'
    admin_iam = boto3.client('iam')
    user_iam = session.client('iam')

    with pytest.raises(ClientError) as ex:
        user_iam.put_role_policy(
            RoleName=vpc_flow_log_role_name,
            PolicyName='Test',
            PolicyDocument=' '
        )

        # if we got here, we need to roll back our change before failing the test
        admin_iam.delete_role_policy(
            RoleName=vpc_flow_log_role_name,
            PolicyName='Test'
        )

        assert False, 'Permissions\niam:*'

    assert 'AccessDenied' in str(ex), 'Error\nlogs:DescribeLogStreams: {}'.format(ex)


def test_create_log_group(vpc_id, user_logs):
    with pytest.raises(ClientError) as ex:
        user_logs.create_log_group(
            logGroupName='{}-FlowLogs'.format(vpc_id),
        )
        assert False, 'Permissions: logs:create*'

    assert 'AccessDeniedException' in str(ex), 'Error: {}'.format(ex)


def test_delete_log_group(vpc_id, user_logs):
    with pytest.raises(ClientError) as ex:
        user_logs.delete_log_group(
            logGroupName='{}-FlowLogs'.format(vpc_id),
        )
        assert False, 'Permissions: logs:delete*'

    assert 'AccessDeniedException' in str(ex), 'Error: {}'.format(ex)


def test_filter_log_events(vpc_id, user_logs):
    with pytest.raises(ClientError) as ex:
        user_logs.filter_log_events(
            logGroupName='{}-FlowLogs'.format(vpc_id),
        )
        assert False, 'Permissions: logs:filterLogEvents'

    assert 'AccessDeniedException' in str(ex), 'Error: {}'.format(ex)


def test_put_log_events(vpc_id, user_logs):
    with pytest.raises(ClientError) as ex:
        user_logs.put_log_events(
            logGroupName='{}-FlowLogs'.format(vpc_id),
            logStreamName='tmp',
            logEvents=[{
                'timestamp': 123,
                'message': 'string',
            }],
        )
        assert False, 'Permissions: logs:put*'

    assert 'AccessDeniedException' in str(ex), 'Error: {}'.format(ex)
