'''
----------------
test_s3endpoint.py
----------------

"Type": "functional",
"Name": "test_s3endpoint",
"Description": "Verify access to S3 through enpoint.",
"Plan": "Create and s3 bucket and ec2 instance. Then see if the ec2 instance can upload a file to the S3 bucket.",
"ExpectedResult": "Success"

Verify access to S3 through endpoint.

Plans:

* create an S3 bucket
* create EC2 instance
* ensure that EC2 instance can upload a file to the S3 bucket

${testcount:4}
'''

import boto3
import pytest

from aws_test_functions import (
    find_objects_in_bucket,
    get_subnet_id,
    is_bucket_empty,
    new_bucket,
    new_ec2_instance,
    new_role,
)


# template for a script each instance will run at boot
USER_DATA = """#!/bin/bash

aws s3 cp /etc/os-release s3://{bucket.name}/{key}
"""


@pytest.fixture(scope='module')
def role():
    with new_role('test_s3endpoint', 'ec2', 'AmazonS3FullAccess') as r:
        yield r


@pytest.fixture(scope='module')
def bucket():
    with new_bucket('test_s3endpoint') as b:
        yield b


@pytest.fixture
def subnet_id(vpc_id):
    return get_subnet_id(vpc_id, 'Public Subnet 1')


@pytest.mark.slowtest
def test_s3endpoint(role, bucket, vpc_id, subnet_id):
    """Upload file to temporary S3 bucket from inside an EC2 instance"""

    dest = '{}-{}-os-release'.format(vpc_id, subnet_id).replace(' ', '-')
    user_data = USER_DATA.format(
        bucket=bucket,
        key=dest,
    )
    print('Rendered user data:\n' + user_data)

    with new_ec2_instance(role=role, SubnetId=subnet_id, UserData=user_data):
        # wait for the user data script to execute
        s3 = boto3.client('s3')
        waiter = s3.get_waiter('object_exists')
        waiter.wait(
            Bucket=bucket.name,
            Key=dest,
            WaiterConfig={
                'Delay': 5,
                'MaxAttempts': 36,
            }
        )

        # just in case the waiter timed out
        assert not is_bucket_empty(bucket.name)

    found = find_objects_in_bucket(bucket.name, dest)
    assert found[dest] is True
