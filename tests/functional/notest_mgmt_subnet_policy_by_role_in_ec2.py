'''
--------------------------
test_mgmt_subnet_policy_by_role_in_ec2.py
--------------------------

"Type": "functional",
"Name": "test_mgmt_subnet_policy_by_role_in_ec2",
"Description": "Can an ec2 instance running with a user created role deploy stuff in the management subnet (ManagementSubnet1 and ManagementSubnet2).",
"Plan": "Create role without the ManagementSubnetPolicy attached to it, create a test user and assign the user to that role, and then perform the following tests that should not be allowed: P1.  Create Ec2 instance, P2. Create an IAM role, P3.  Launch the instance using that role, P4.  Try to launch another copy of the ec2 instance into the ManagementSubnet (ManagementSubnet1 or ManagementSubnet2)",
"ExpectedResult": "Failure"

${testcount:1}
'''

import boto3
from aws_test_functions import *
from botocore.exceptions import ClientError
from time import sleep

import pprint

def get_userdata_script(test_mgmt_subnet):
    return '''
#!/bin/bash

echo "Starting Script"

while true
do
    aws ec2 --region us-east-1 create-tags --resources $(curl http://169.254.169.254/latest/meta-data/instance-id) --tags Key=TestResults,Value=Ready
    results=$?
    echo "create tag: $results"
    if [ $results -eq 0 ]; then
        break
    fi
    sleep 10
done
while true
do
    sleep 1
    aws ec2 --region us-east-1 describe-tags --filters "Name=resource-id,Values=$(curl http://169.254.169.254/latest/meta-data/instance-id)" | grep 'Awaiting Results...'
    if [ "$?" == "0" ]; then
        echo "broke out looking for 'Awaiting Results...' "
        break
    fi
done


while true
do
    sleep 1
    aws ec2 --region us-east-1 run-instances --image-id ami-6057e21a --instance-type t2.micro --subnet-id ''' +  str(test_mgmt_subnet) + ''' --dry-run --count 1  2> ./tmp.txt
    cat ./tmp.txt
    grep "UnauthorizedOperation" tmp.txt
    if [ $? -eq 0 ]; then
        echo "Passed Test. Break out and change tag"
        while true
        do 
            sleep 10
            aws ec2 --region us-east-1 create-tags --resources $(curl http://169.254.169.254/latest/meta-data/instance-id) --tags Key=TestResults,Value=Passed
        done
    fi
    grep "DryRunOperation" tmp.txt
    if [ $? -eq 0 ]; then
        echo "Failed Test. Break out and change tag"
        while true
        do 
            sleep 10
            aws ec2 --region us-east-1 create-tags --resources $(curl http://169.254.169.254/latest/meta-data/instance-id) --tags Key=TestResults,Value=Failed
        done
    fi
    grep "Unable to locate credentials" tmp.txt
    if [ $? -eq 0 ]; then
        echo "Waiting for creds"
    fi

done
echo "Finished Script"
                '''

def failed_test(resource, reason):
    print(resource, "\n", reason, "\n")
    assert False

def passed_test(resource, reason):
    print(resource, "\n", reason, "\n")
    assert True

def get_managed_vpc(admin_ec2):
    managed_vpcs = []
    response = admin_ec2.describe_vpcs()
    for vpc in response['Vpcs']:
        if 'Tags' in vpc:
            for tag in vpc['Tags']:
                if tag['Key'] == 'RHEDcloudVpcType':
                    managed_vpcs.append(vpc['VpcId'])
    return managed_vpcs

def test_answer():
    admin_ec2 = boto3.client('ec2')
    admin_iam = boto3.client('iam')

    # Attachment delay to exploit the delta between IAM creation and utilizing the IAM resource
    attachment_delay = 0
    timeout_value = 240

    for mgmt_vpc in get_managed_vpc(admin_ec2):
        user = create_test_user()
        rhedcloudAdminRolePolicyArn = "arn:aws:iam::"+get_account_number()+":policy/RHEDcloudAdministratorRolePolicy"
        rhedcloudSubnetPolicyArn = "arn:aws:iam::"+get_account_number()+":policy/" + get_stack_name_from_vpc_id(mgmt_vpc) + "-RHEDcloudManagementSubnetPolicy"
        attach_policy_to_user(user['UserName'],rhedcloudSubnetPolicyArn)
        attach_policy_to_user(user['UserName'],rhedcloudAdminRolePolicyArn)
        session = create_session_for_test_user(user['UserName'])
        user_ec2 = session.client('ec2')
        user_iam = session.client('iam')
        print(mgmt_vpc)
                             
        subnet_id = None
        subnet_type = None
        new_instance_id = None

        tmp1_instance_profile_creation_check = None
        tmp1_instance_profile_role_creation_check = None
        tmp1_instance_profile_role_name = 'tmp1_instance_profile_role_name'
        tmp1_instance_profile_name = 'tmp1_instance_profile_name'
        tmp1_role_profile_attachment_check = None
        tmp1_instance_profile_role_policy_attachment_check = None

        tmp2_instance_profile_creation_check = None
        tmp2_instance_profile_role_creation_check = None
        tmp2_instance_profile_role_name = 'tmp2_instance_profile_role_name'
        tmp2_instance_profile_name = 'tmp2_instance_profile_name'
        tmp2_role_profile_attachment_check = None
        tmp2_instance_profile_role_policy_attachment_check = None

        test_subnet = None
        test_mgmt_subnet = None
        new_igw_creation_check = None
        new_igw_id = None
        new_igw_attachment_check = None
        new_route_table_creation_check = None
        old_route_table_id = None
        new_route_table_id = None
        new_route_table_assoc_check = None
        new_route_table_assoc_id = None
        old_route_table_assoc_id = None
        default_acl_id = None
        old_network_acl_assoc_id = None
        new_network_acl_assoc_id = None
        old_acl_id = None
        new_acl_attachment_check = None
        role_trust_policy="""{
            "Version": "2012-10-17",
            "Statement": [
                {
                "Effect": "Allow",
                "Principal": {
                    "Service": "ec2.amazonaws.com"
                },
                "Action": "sts:AssumeRole"
                }
            ]
        }"""

        try:
            try:
                response = user_ec2.describe_subnets(
                    Filters=[
                        {
                            'Name': 'vpc-id',
                            'Values': [
                                mgmt_vpc,
                            ],
                        },
                    ],
                )
                
                for subnet in response['Subnets']:
                    subnet_id = subnet['SubnetId']
                    if 'Tags' in subnet:
                        for tag in subnet['Tags']:
                            if tag['Key'] == 'Name':
                                if tag['Value'].startswith('Public'):
                                    subnet_type = 'public'
                                    test_subnet = subnet_id
                                if tag['Value'].startswith('Private'):
                                    subnet_type = 'private'
                                if tag['Value'].startswith('MGMT'):
                                    test_mgmt_subnet = subnet_id
                                    subnet_type = 'mgmt'
                
                # Create instance profiles
                response = user_iam.create_instance_profile(
                    InstanceProfileName=tmp2_instance_profile_name
                )
                tmp2_instance_profile_creation_check = 'yes'
                sleep(20)

                response = user_iam.create_role(
                    RoleName=tmp2_instance_profile_role_name,
                    AssumeRolePolicyDocument=role_trust_policy,
                    Description='Test Instance Role'
                )
                tmp2_instance_profile_role_creation_check = 'yes'

                response = user_iam.attach_role_policy(
                    RoleName=tmp2_instance_profile_role_name,
                    PolicyArn='arn:aws:iam::aws:policy/AmazonEC2FullAccess'
                )
                tmp2_instance_profile_role_policy_attachment_check = 'yes'

                response = user_iam.add_role_to_instance_profile(
                    InstanceProfileName=tmp2_instance_profile_name,
                    RoleName=tmp2_instance_profile_role_name
                )
                tmp2_role_profile_attachment_check = 'yes'

                response = user_iam.create_instance_profile(
                    InstanceProfileName=tmp1_instance_profile_name
                )
                tmp1_instance_profile_creation_check = 'yes'
                print("instance profiles created")

                response = admin_ec2.create_internet_gateway()
                new_igw_id = response['InternetGateway']['InternetGatewayId']
                new_igw_creation_check = "yes"
                print("Created IGW")

                response = admin_ec2.attach_internet_gateway(
                    InternetGatewayId=new_igw_id,
                    VpcId=mgmt_vpc
                )
                new_igw_attachment_check = 'yes'
                print("Attached IGW")

                response = admin_ec2.create_route_table(
                    VpcId=mgmt_vpc
                )
                new_route_table_id = response['RouteTable']['RouteTableId']
                new_route_table_creation_check = 'yes'
                print("Created Route Table")

                response = admin_ec2.describe_route_tables()
                for table in response['RouteTables']:
                    for assoc in table['Associations']:
                        if 'SubnetId' in assoc:
                            if assoc['SubnetId'] == test_subnet:
                                old_route_table_id = assoc['RouteTableId']
                                old_route_table_assoc_id = assoc['RouteTableAssociationId']
                
                response = admin_ec2.replace_route_table_association(
                    AssociationId=old_route_table_assoc_id,
                    RouteTableId=new_route_table_id
                )
                new_route_table_assoc_id = response['NewAssociationId']
                new_route_table_assoc_check = 'yes'

                response = admin_ec2.create_route(
                    DestinationCidrBlock='0.0.0.0/0',
                    GatewayId=new_igw_id,
                    RouteTableId=new_route_table_id
                )

                response = admin_ec2.describe_network_acls(
                    Filters=[
                        {
                            'Name': 'vpc-id',
                            'Values': [
                                mgmt_vpc,
                            ]
                        },
                    ]
                )
                for acl in response['NetworkAcls']:
                    if acl['IsDefault'] == True:
                        default_acl_id = acl['NetworkAclId']
                    if acl['IsDefault'] == False:
                        for assoc in acl['Associations']:
                            if assoc['SubnetId'] == test_subnet:
                                old_acl_id = assoc['NetworkAclId']
                                old_network_acl_assoc_id = assoc['NetworkAclAssociationId']

                response = admin_ec2.replace_network_acl_association(
                    AssociationId=old_network_acl_assoc_id,
                    NetworkAclId=default_acl_id
                )
                new_network_acl_assoc_id = response['NewAssociationId']
                new_acl_attachment_check = 'yes'

                response = user_ec2.run_instances(
                    BlockDeviceMappings=[],
                    ImageId='ami-6057e21a',
                    InstanceType='t2.micro',
                    MaxCount=1,
                    MinCount=1,
                    Monitoring={
                        'Enabled': False
                    },
                    NetworkInterfaces=[
                        {
                        'DeviceIndex': 0,
                        'AssociatePublicIpAddress': True,
                        'SubnetId': test_subnet
                        },
                    ],
                    IamInstanceProfile={
                        'Arn': 'arn:aws:iam::' + get_account_number() + ':instance-profile/' + tmp2_instance_profile_name
                    },
                    UserData=get_userdata_script(test_mgmt_subnet),
                    TagSpecifications=[
                        {
                            'ResourceType': 'instance',
                            'Tags': [
                                {
                                    'Key': 'TestResults',
                                    'Value': 'Not Ready...'
                                },
                                {
                                    'Key': 'Name',
                                    'Value': 'test_mgmt_subnet_policy_by_role_in_ec2'
                                }
                            ]
                        },
                    ]
                )
                print("instance created")
                new_instance_id = response['Instances'][0]['InstanceId']

                # Check to see if instance is ready
                loop = True
                time = 0
                while loop is True:
                    response = user_ec2.describe_tags(
                        DryRun=False,
                        Filters=[
                            {
                                'Name': 'resource-id',
                                'Values': [
                                    new_instance_id,
                                ]
                            }
                        ]
                    )
                    key = None
                    value = None
                    for tag in response['Tags']:
                        if tag['Key'] == 'TestResults':
                            key = tag['Key']
                            value = tag['Value']
                    if value == 'Ready':
                        loop = False
                    if value == 'Not Ready...':
                        print("Waiting to be ready...")
                        if time >= timeout_value:
                            failed_test('Error', 'Timed-out: Waiting for instance to be \'Ready\' timed out. Networking issue.')
                    time += 5
                    sleep(5)

                response = user_ec2.describe_iam_instance_profile_associations(
                    Filters=[
                        {
                            'Name': 'instance-id',
                            'Values': [
                                new_instance_id
                            ]
                        },
                    ]
                )
                association_id = response['IamInstanceProfileAssociations'][0]['AssociationId']
                response = user_ec2.disassociate_iam_instance_profile(AssociationId=association_id)

                response = user_iam.create_role(
                    RoleName=tmp1_instance_profile_role_name,
                    AssumeRolePolicyDocument=role_trust_policy,
                    Description='Test Instance Role'
                )
                tmp1_instance_profile_role_creation_check = 'yes'

                response = user_iam.attach_role_policy(
                    RoleName=tmp1_instance_profile_role_name,
                    PolicyArn='arn:aws:iam::aws:policy/AmazonEC2FullAccess'
                )
                tmp1_instance_profile_role_policy_attachment_check = 'yes'

                response = user_iam.add_role_to_instance_profile(
                    InstanceProfileName=tmp1_instance_profile_name,
                    RoleName=tmp1_instance_profile_role_name
                )
                tmp1_role_profile_attachment_check = 'yes'

                response = user_ec2.associate_iam_instance_profile(
                    IamInstanceProfile={
                        'Arn': 'arn:aws:iam::' + get_account_number() + ':instance-profile/' + tmp1_instance_profile_name,
                        'Name':tmp1_instance_profile_name
                    },
                    InstanceId=new_instance_id
                )

                # Delay to try to abuse existing blocks
                sleep(attachment_delay)

                print("Sleep finished")

                # Run userscript code
                response = user_ec2.create_tags(
                    Resources=[
                        new_instance_id
                    ],
                    Tags=[
                        {
                            'Key': 'TestResults',
                            'Value': 'Awaiting Results...',
                        },
                    ],
                )

                print("Did IAM calls, waiting for userscript results...")

                # Check to see if Tag has been changed
                loop = True
                time = 0
                while loop is True:
                    print("Waiting results...")
                    response = user_ec2.describe_tags(
                        DryRun=False,
                        Filters=[
                            {
                                'Name': 'resource-id',
                                'Values': [
                                    new_instance_id,
                                ]
                            }
                        ]
                    )
                    key = None
                    value = None
                    for tag in response['Tags']:
                        if tag['Key'] == 'TestResults':
                            key = tag['Key']
                            value = tag['Value']
                    if time >= timeout_value:
                        failed_test('Error', 'Timed-out: Waiting for instance to set test results timed out. Networking issue.')
                    if value == 'Failed':
                        # print("This test failed, VPC: " + mgmt_vpc)
                        # loop = False
                        failed_test('Instance', 'Creates instance in MGMT subnet. VPC: '+mgmt_vpc+' Tested Subnet: '+test_mgmt_subnet )
                    if value == 'Passed':
                        loop = False
                    sleep(5)
                    time += 5
            except ClientError as e:
                if e.response['Error']['Code'] == 'AccessDenied':
                    failed_test('Script', str(e))
                elif e.response['Error']['Code'] == 'UnauthorizedOperation':
                    failed_test('Error', 'Permissions: ' + str(e))
                elif e.response['Error']['Code'] == 'DryRunOperation':
                    print("Expected error, test successful.")
                elif e.response['Error']['Code'] == 'EntityAlreadyExists':
                    if tmp2_instance_profile_name in str(e):
                        tmp2_instance_profile_creation_check = 'yes'
                    if tmp1_instance_profile_name in str(e):
                        tmp1_instance_profile_creation_check = 'yes'
                    if tmp2_instance_profile_role_name in str(e):
                        tmp2_instance_profile_role_creation_check = 'yes'
                    if tmp1_instance_profile_role_name in str(e):
                        tmp1_instance_profile_role_creation_check = 'yes'
                    failed_test('EntityAlreadyExists', 'Rerun Test: ' + str(e))
                else:
                    failed_test('Error', 'Resource: ' + str(e))
        finally:
            print("Finally")

            if user:
                deleteTestUser(user['UserName'])
                print("Deleted: " + user['UserName'])
            
            if new_instance_id != None:
                response = admin_ec2.terminate_instances(
                    InstanceIds=[
                        new_instance_id,
                    ]
                )
                print("Terminated Instance. Remember to comment out this block if instance needs debugging.")

            if tmp1_role_profile_attachment_check != None:
                response = admin_iam.remove_role_from_instance_profile(
                    InstanceProfileName=tmp1_instance_profile_name,
                    RoleName=tmp1_instance_profile_role_name
                )
                print("Detached Role from Instance Profile")

            if tmp1_instance_profile_role_policy_attachment_check != None:
                response = admin_iam.detach_role_policy(
                    RoleName=tmp1_instance_profile_role_name,
                    PolicyArn='arn:aws:iam::aws:policy/AmazonEC2FullAccess'
                )
                print("Removed Policy From: " + tmp1_instance_profile_role_name)

            if tmp1_instance_profile_role_creation_check != None:
                response = admin_iam.delete_role(
                    RoleName=tmp1_instance_profile_role_name
                )
                print("Deleted: " + tmp1_instance_profile_role_name)
            
            if tmp1_instance_profile_creation_check != None:
                response = admin_iam.delete_instance_profile(
                    InstanceProfileName=tmp1_instance_profile_name
                )
                print("Deleted: " + tmp1_instance_profile_name)

            # Second set of instance profile items

            if tmp2_role_profile_attachment_check != None:
                response = admin_iam.remove_role_from_instance_profile(
                    InstanceProfileName=tmp2_instance_profile_name,
                    RoleName=tmp2_instance_profile_role_name
                )
                print("Detached Tmp Role from Instance Profile")


            if tmp2_instance_profile_role_policy_attachment_check != None:
                response = admin_iam.detach_role_policy(
                    RoleName=tmp2_instance_profile_role_name,
                    PolicyArn='arn:aws:iam::aws:policy/AmazonEC2FullAccess'
                )
                print("Removed Policy From: " + tmp2_instance_profile_role_name)


            if tmp2_instance_profile_role_creation_check != None:
                response = admin_iam.delete_role(
                    RoleName=tmp2_instance_profile_role_name
                )
                print("Deleted: " + tmp2_instance_profile_role_name)
            
            if tmp2_instance_profile_creation_check != None:
                response = admin_iam.delete_instance_profile(
                    InstanceProfileName=tmp2_instance_profile_name
                )
                print("Deleted: " + tmp2_instance_profile_name)

            # Added infrastructure

            if new_acl_attachment_check != None:
                response = admin_ec2.replace_network_acl_association(
                    AssociationId=new_network_acl_assoc_id,
                    NetworkAclId=old_acl_id
                )
                print("Replaced Network ACL")

            if new_route_table_assoc_check != None:
                response = admin_ec2.replace_route_table_association(
                    AssociationId=new_route_table_assoc_id,
                    RouteTableId=old_route_table_id
                )
                print("Disassociated Routing Table")

            if new_route_table_creation_check != None:
                response = admin_ec2.delete_route_table(
                    RouteTableId=new_route_table_id
                )
                print("Deleted Routing Table")

            if new_igw_attachment_check != None:
                loop = True

                if new_instance_id != None:
                    while loop is True:
                        sleep(5)
                        response = admin_ec2.describe_instances(
                            InstanceIds=[
                                new_instance_id
                            ]
                        )
                        instance_state = response['Reservations'][0]['Instances'][0]['State']['Name']
                        print("Instance State: " + instance_state)
                        if instance_state == 'terminated':
                            loop = False


                response = admin_ec2.detach_internet_gateway(
                    InternetGatewayId=new_igw_id,
                    VpcId=mgmt_vpc
                )
                print("Detached IGW")

            if new_igw_creation_check != None:
                response = admin_ec2.delete_internet_gateway(
                    InternetGatewayId=new_igw_id
                )
                print("Deleted IGW")
    passed_test("Passed", "Passed")