'''
--------------------------
test_mgmt_subnet_policy_by_role.py
--------------------------

"Type": "functional",
"Name": "test_mgmt_subnet_policy_by_role",
"Description": "This test performs a series of positive and negative checks to verify the RHEDcloudAdministratorRole role can not perform ec2 operations in either management subnet, but are allowed to perform ec2 operations in non-management subnets.",
"Plan": "Create role without the ManagementSubnetPolicy attached to it, create a test user and assign the user to that role, and then perform the following tests that should be allowed: P1. Create ec2 instances in the following subnets: PublicSubnet1, PublicSubnet2, PrivateSubnet1, and PrivateSubnet2; and P2. Terminate the ec2 instances that were created in P1. Then perform the following negative test on each of the management subnets: N1. Attempt to create ec2 instances in each management subnet (ManagementSubnet1 and ManagementSubnet2).  NOTE:  This test outlines a possible vulnerability in our current security approach because we think the user that has assumed the role mentioned above WILL actually be able to perform these functions which they should not be allowed to perform.",
"ExpectedResult": "Failure"
 
${testcount:6}

'''

import boto3
from aws_test_functions import *
from botocore.exceptions import ClientError

import pprint

def failed_test(resource, reason):
    print(resource, "\n", reason, "\n")
    assert False

def passed_test(resource, reason):
    print(resource, "\n", reason, "\n")
    assert True

def get_managed_vpc(admin_ec2):
    managed_vpcs = []
    response = admin_ec2.describe_vpcs()
    for vpc in response['Vpcs']:
        if 'Tags' in vpc:
            for tag in vpc['Tags']:
                if tag['Key'] == 'RHEDcloudVpcType':
                    managed_vpcs.append(vpc['VpcId'])
    return managed_vpcs
                    

def test_answer():
    user = create_test_user()
    rhedcloudAdminRolePolicyArn = "arn:aws:iam::"+get_account_number()+":policy/RHEDcloudAdministratorRolePolicy"
    rhedcloudSubnetPolicyArn = "arn:aws:iam::"+get_account_number()+":policy/RHEDcloudManagementSubnetPolicy"
    attach_policy_to_user(user['UserName'],rhedcloudAdminRolePolicyArn)
    session = create_session_for_test_user(user['UserName'])
    admin_ec2 = boto3.client('ec2')
    user_ec2 = session.client('ec2')
    admin_sts = boto3.client('sts')
    admin_iam = boto3.client('iam')
    user_iam = session.client('iam')
    launched_instances = []
    admin_delete_instances = None
    new_role_creation_check = None
    new_role_policy_attachment_check = None
    new_policy_creation_check = None
    new_role_policy_creation_check = None

    mgmt_vpc = None
    subnet_id = None
    subnet_type = None
    new_policy_arn = None
    new_role_policy_arn = None

    role_assume_trust_policy = '''{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Effect": "Allow",
      "Principal": {
        "AWS": "arn:aws:iam::''' + get_account_number() + ''':user/''' + user['UserName'] + '''"
      },
      "Action": "sts:AssumeRole"
    }
  ]
}'''

    role_assume_policy = '''{
    "Version": "2012-10-17",
    "Statement": [
        {
            "Sid": "VisualEditor0",
            "Effect": "Allow",
            "Action": "sts:AssumeRole",
            "Resource": "arn:aws:iam::''' + get_account_number() +''':role/test"
        }
    ]
}'''

    role_assume_role_policy = '''{
    "Version": "2012-10-17",
    "Statement": [
        {
            "Sid": "VisualEditor0",
            "Effect": "Allow",
            "Action": "iam:PassRole",
            "Resource": "arn:aws:iam::''' + get_account_number() + ''':user/''' + user['UserName'] + '''"
        }
    ]
}'''

    try:
        try:

            sleep(5)

            response = user_iam.create_policy(
                PolicyName='testpolicy',
                Path='/',
                PolicyDocument=role_assume_policy,
                Description='test_mgmt_subnet_policy_by_role.py. Please delete'
            )
            new_policy_arn = response['Policy']['Arn']
            new_policy_creation_check = 'yes'


            response = user_iam.create_policy(
                PolicyName='testrolepolicy',
                Path='/',
                PolicyDocument=role_assume_role_policy,
                Description='test_mgmt_subnet_policy_by_role.py. Please delete'
            )
            new_role_policy_arn = response['Policy']['Arn']
            new_role_policy_creation_check = 'yes'


            attach_policy_to_user(user['UserName'],new_policy_arn)

            response = user_iam.create_role(
                Path='/',
                RoleName='test',
                AssumeRolePolicyDocument=role_assume_trust_policy,
                Description='test_mgmt_subnet_policy_by_role.py. Please delete'
            )
            new_role_creation_check = 'yes'

            response = user_iam.attach_role_policy(
                RoleName='test',
                PolicyArn='arn:aws:iam::aws:policy/AdministratorAccess'
            )

            response = user_iam.attach_role_policy(
                RoleName='test',
                PolicyArn=new_role_policy_arn
            )
            new_role_policy_attachment_check = 'yes'

            sleep(15)
            user_sts = session.client('sts')

            response = user_sts.assume_role(
                DurationSeconds=900,
                RoleArn='arn:aws:iam::' + get_account_number() + ':role/test',
                RoleSessionName='testpipeline',
            )
            temp_access_key = response['Credentials']['AccessKeyId']
            temp_secret_key = response['Credentials']['SecretAccessKey']
            temp_session_token = response['Credentials']['SessionToken']
            print("Assumed role: " + response['AssumedRoleUser']['Arn'])

            role_ec2 = boto3.client(
                'ec2',
                aws_access_key_id=temp_access_key,
                aws_secret_access_key=temp_secret_key,
                aws_session_token=temp_session_token,
            )
            print("Created client for role")

            for mgmt_vpc in get_managed_vpc(user_ec2):
                response = user_ec2.describe_subnets(
                    Filters=[
                        {
                            'Name': 'vpc-id',
                            'Values': [
                                mgmt_vpc,
                            ],
                        },
                    ],
                )
                for subnet in response['Subnets']:
                    subnet_id = subnet['SubnetId']
                    if 'Tags' in subnet:
                        for tag in subnet['Tags']:
                            if tag['Key'] == 'Name':
                                if tag['Value'].startswith('Public'):
                                    subnet_type = 'public'
                                if tag['Value'].startswith('Private'):
                                    subnet_type = 'private'
                                if tag['Value'].startswith('MGMT'):
                                    subnet_type = 'mgmt'
                        if subnet_type == 'public' or subnet_type == 'private':
                            try:
                                response = role_ec2.run_instances(
                                    BlockDeviceMappings=[],
                                    ImageId='ami-6057e21a',
                                    InstanceType='t2.micro',
                                    MaxCount=1,
                                    MinCount=1,
                                    Monitoring={
                                        'Enabled': False
                                    },
                                    SubnetId=subnet_id,
                                    TagSpecifications=[
                                        {
                                            'ResourceType': 'instance',
                                            'Tags': [
                                                {
                                                    'Key': 'Name',
                                                    'Value': 'test_mgmt_subnet_policy_by_role_' + subnet_id 
                                                }
                                            ]
                                        },
                                    ]
                                )
                                launched_instances.append(response['Instances'][0]['InstanceId'])
                                admin_delete_instances = 'yes'
                            except ClientError as e:
                                if e.response['Error']['Code'] == 'UnauthorizedOperation':
                                    failed_test('Error', subnet_id + ': ' + str(e))
                                else:
                                    failed_test('Error', subnet_id + ': ' + str(e))
                        if subnet_type == 'mgmt':
                            try:
                                response = role_ec2.run_instances(
                                    BlockDeviceMappings=[],
                                    ImageId='ami-6057e21a',
                                    InstanceType='t2.micro',
                                    MaxCount=1,
                                    MinCount=1,
                                    Monitoring={
                                        'Enabled': False
                                    },
                                    SubnetId=subnet_id,
                                    DryRun=True,
                                    TagSpecifications=[
                                        {
                                            'ResourceType': 'instance',
                                            'Tags': [
                                                {
                                                    'Key': 'Name',
                                                    'Value': 'test_mgmt_subnet_policy_by_role_' + subnet_id 
                                                }
                                            ]
                                        },
                                    ]
                                )
                            except ClientError as e:
                                if e.response['Error']['Code'] == 'UnauthorizedOperation':
                                    print("Expected error, test successful.")
                                else:
                                    failed_test('Error', subnet_id + ': ' + str(e))
            try:
                print(launched_instances)
                response = role_ec2.terminate_instances(
                    InstanceIds=launched_instances
                )
                admin_delete_instances = None
            except ClientError as e:
                if e.response['Error']['Code'] == 'AccessDenied':
                    failed_test('Instances', 'Deleting instances as User')
                else:
                    failed_test('Error', 'Instances: ' + str(e))
        except ClientError as e:
            if e.response['Error']['Code'] == 'AccessDenied':
                failed_test('User', 'Permissions')
            else:
                failed_test('Error', 'Resource: ' + str(e))
        passed_test("Passed", "Passed")
    finally:
        print("Finally")

        if user:
            deleteTestUser(user['UserName'])
            print("Deleted: " + user['UserName'])

        if admin_delete_instances != None:
            response = admin_ec2.terminate_instances(
                InstanceIds=launched_instances
            )
            print("Deleted: " + str(launched_instances))

        if new_role_policy_attachment_check != None:
            response = admin_iam.detach_role_policy(
                RoleName='test',
                PolicyArn='arn:aws:iam::aws:policy/AdministratorAccess'
            )

            response = admin_iam.detach_role_policy(
                RoleName='test',
                PolicyArn=new_role_policy_arn
            )
            print("Detached Policies From Role")

        if new_role_creation_check != None:
            response = admin_iam.delete_role(
                RoleName='test'
            )
            print("Deleted Test Role")

        if new_policy_creation_check != None:
            response = admin_iam.delete_policy(
                PolicyArn=new_policy_arn
            )
            print("Deleted Test Policy")

        if new_role_policy_creation_check != None:
            response = admin_iam.delete_policy(
                PolicyArn=new_role_policy_arn
            )
            print("Deleted Test Policy 2")

