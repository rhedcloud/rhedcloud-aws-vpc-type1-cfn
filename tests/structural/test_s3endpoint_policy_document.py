'''
----------------------------------
test_s3endpoint_policy_document.py
----------------------------------

"Type": "structural",
"Name": "test_s3endpoint_policy_document",
"Description": "Verify the vpc endpoint policy is full-access.",
"Plan": "Describe the vpc endpoint and verify the policy document matches expected document.",
"ExpectedResult": "Success"

This test verifies that the proper policy appears in the enpoint policy document by comparing the deployed policy with the expected bucket policy.  PNP
'''

import json

from aws_test_functions import (
    aws_client,
    dict_to_filters,
)


@aws_client('ec2')
def get_s3endpoint_policy_document(vpc_id, *, ec2=None):
    endpoint = ec2.describe_vpc_endpoints(Filters=dict_to_filters({
        'vpc-id': vpc_id,
        'service-name': 'com.amazonaws.us-east-1.s3',
    }))

    return json.loads(endpoint['VpcEndpoints'][0]['PolicyDocument'])


def test_answer(vpc_id):
    endpoint_policy = {
        "Version": "2012-10-17",
        "Statement": [
            {
                "Effect": "Allow",
                "Principal": "*",
                "Action": "*",
                "Resource": "*"
            }
        ]
    }

    assert get_s3endpoint_policy_document(vpc_id) == endpoint_policy
