'''
----------------------------
test_rhedcloud_vpn2_state.py
----------------------------

"Type": "structural",
"Name": "test_rhedcloud_vpn2_state",
"Description": "Verify the vpn state is available.",
"Plan": "Describe the VPN connecting to AWS Research VPC VPN Endpoint 2, by using tags, and check state.",
"ExpectedResult": "Success"
'''
import pytest
from aws_test_functions import aws_client


@aws_client('ec2')
def check_vpn_connection_state(vpc_id, tag_name, ec2=None):
    f = {'Name': 'tag:Name', 'Values': [tag_name]}
    for conn in ec2.describe_vpn_connections(Filters=[f])['VpnConnections']:
        id = conn['VpnGatewayId']
        gw = ec2.describe_vpn_gateways(VpnGatewayIds=[id])['VpnGateways'][0]
        for a in gw['VpcAttachments']:
            if a['VpcId'] != vpc_id:
                continue
            if conn['State'] != 'available' or a['State'] != 'attached':
                return False
    return True

@pytest.mark.vpn_test
def test_answer(vpc_id):
    assert check_vpn_connection_state(vpc_id, 'RHEDcloudVpnConnection2')
