'''
-----------------
test_vpcflowlogs_policy_attached.py
-----------------

"Type": "structural",
"Name": "test_vpcflowlogs_policy_attached",
"Description": "Verify that the VpcFlowLogsPolicy is attached to the RHEDcloudAdministratorRole.",
"Plan": "Look up the RHEDcloudAdministratorRole role by name, get a list of the attached policies, and verify that the RHEDcloudVpcFlowLogsPolicy is attached.",
"ExpectedResult": "Success"

This test verifies that the RHEDcloudVpcFlowLogsPolicy is attached to the RHEDcloudAdministratorRole
by verifying that the correct policy ARN is returned when
querying for attached policies.

'''

from aws_test_functions import (
    ClientError,
    aws_resource,
    catch,
    find_policy_for_stack,
    get_stack_name_from_vpc_id,
)


@catch(ClientError, return_value=False,
       msg='Unexpected error when checking if policy attached to role.\n{ex}')
@aws_resource('iam')
def is_policy_attached_to_role(role_name, policy_arn, *, iam=None):
    role = iam.Role(role_name)
    for apolicy in role.attached_policies.all():
        if apolicy.arn == policy_arn:
            return True

    print("Cannot find policy attached to the role\n")
    return False


def test_policy_attached(vpc_id):
    stack_name = get_stack_name_from_vpc_id(vpc_id)
    arn = find_policy_for_stack(stack_name, 'RHEDcloudVpcFlowLogsPolicy')

    assert is_policy_attached_to_role('RHEDcloudAdministratorRole', arn)
