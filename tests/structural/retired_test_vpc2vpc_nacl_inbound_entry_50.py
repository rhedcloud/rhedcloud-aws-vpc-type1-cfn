'''
----------------
test_vpc2vpc_nacl_inbound_entry_50.py
----------------

"Type": "structural",
"Name": "test_vpc2vpc_nacl_inbound_entry_50",
"Description": "Verify the NACL entry exists.",
"Plan": "Describe the Network ACL (need nacl-id) and verify there is an entry that matches: CidrBlock=VPC CIDR, RuleNumber=50, Protocol=-1, Egress=false, and RuleAction=allow.",
"ExpectedResult": "Success"

This test loops through all of the VPCs of a particular type (specified as a parameter that is passed in)
and checks to make sure NACL inbound entry rule number 50 is present.
'''

from aws_test_functions import check_vpc2vpc_nacl_entry, get_vpc_cidr


def test_answer(vpc_id):
    assert check_vpc2vpc_nacl_entry(vpc_id, {
        'CidrBlock': get_vpc_cidr(vpc_id),
        'Egress': False,
        'Protocol': '-1',
        'RuleAction': 'allow',
        'RuleNumber': 50,
    })
