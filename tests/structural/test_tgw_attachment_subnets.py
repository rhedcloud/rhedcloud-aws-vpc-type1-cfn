'''
-----------------
test_tgw_attachment_subnets.py
-----------------

"Type": "structural",
"Name": "test_tgw_attachment_subnets",
"Description": "Verify the TGW Attachment network interfaces are in the Management Subnets",
"Plan": "Describe the network interfaces in the Management Subnets and make sure there are two with a description containing the TGW Attachment ID.",
"ExpectedResult": "Success"

'''

import pytest
from aws_test_functions import aws_client, dict_to_filters, get_subnet_id

@aws_client('ec2')
def check_tgw_attachment_interfaces(vpc_id, *, ec2=None):

    attachment1 = False
    attachment2 = False 


    """Getting the transit gateway attachment ID in the current account."""
    tgw_response = ec2.describe_transit_gateway_vpc_attachments(Filters=dict_to_filters({
        'vpc-id': vpc_id,
    }))

    tgw_attachment_id = tgw_response['TransitGatewayVpcAttachments'][0]['TransitGatewayAttachmentId']
        
    print(tgw_attachment_id)

    """Getting management sunbets IDs"""
    mgmt1_subnet_id = get_subnet_id(vpc_id, 'MGMT1 - IT ONLY')

    mgmt2_subnet_id = get_subnet_id(vpc_id, 'MGMT2 - IT ONLY')


    """Getting network interfaces within management subnet 1"""
    network_interface_from_mgmt_subnet_1 = ec2.describe_network_interfaces(Filters=dict_to_filters({
        'subnet-id': mgmt1_subnet_id,
    }))

    for network_interface1 in network_interface_from_mgmt_subnet_1['NetworkInterfaces']:
        if tgw_attachment_id in network_interface1['Description']:
            attachment1 = True

    """Getting network interfaces within management subnet 2"""
    network_interface_from_mgmt_subnet_2 = ec2.describe_network_interfaces(Filters=dict_to_filters({
        'subnet-id': mgmt2_subnet_id,
    }))

    for network_interface2 in network_interface_from_mgmt_subnet_2['NetworkInterfaces']:
        if tgw_attachment_id in network_interface2['Description']:
            attachment2 = True

    """Return true if both attachments are true; otherwise return false"""
    if attachment1 and attachment2:
        return True
    else:
        return False

@pytest.mark.tgw_test
def test_answer(vpc_id):
    assert check_tgw_attachment_interfaces(vpc_id)
