"""
==================================
test_elasticloadbalancing_endpoint
==================================

"Type": "structural",
"Name": "test_elasticloadbalancing_endpoint",
"Description": "Verify the service name, state, and availability zones.",
"Plan": "Service name should equal com.amazonaws.[REGION].elasticloadbalancing; state should be available; and there should be two NetworkInterfaceIds",
"ExpectedResult": "Success"

"""

import pytest

from tests.structural.vpc_endpoint_tests import *


@pytest.fixture(scope='module')
def endpoint_name():
    return "elasticloadbalancing"
