'''
-------------------------------
test_tgw_mgmt_route_table_routes.py
-------------------------------

"Type": "structural",
"Name": "test_tgw_mgmt_route_table_routes",
"Description": "Verify there are three active routes in the route table and check to make sure they are the right ones.",
"Plan": "Describe the route table (need subnet ID) and verify the routes are correct (local - desitination CIDR block matches VPC CIDR block & GatewayId=local, default - desination CIDR Block is 0.0.0.0/0 & GatewayId matches IGW, and S3 endpoint - desitnation prefix begins with 'pl-') and active.",
"ExpectedResult": "Success"

Since the routes for TGW-attached VPCs use an Internet Gateway Id rather than the VGW GatewayId, the expected results differ from VPN connected VPCs.

Verify there are three active routes in the route table and check to make sure they are the right ones.
Describe the route table (need subnet ID) and verify the routes are correct
local - desitination CIDR block matches VPC CIDR block & GatewayId=local,
default - desination CIDR Block is 0.0.0.0/0 & GatewayId matches VGW or TransitGatewayID matches TGW,
S3 endpoint - desitnation prefix begins with 'pl-') and active.
'''
import pytest
from aws_test_functions import (
    ClientError,
    catch,
    aws_client,
    aws_resource,
    dict_to_filters,
    get_vpc_route_table,
)

ROUTE_TABLE_NAME = 'Management Route Table'

@catch(ClientError, return_value='',
       msg='Unexpected error when getting VPC Cidr Block: {ex}')
@aws_resource('ec2')
def get_vpc_cidr(subnet_id, *, ec2=None):
    '''
    Getting the CIDR range associated with the VPC
    '''

    subnet_vpc = ec2.Subnet(subnet_id).vpc_id

    print('vpc id' + subnet_vpc)
    vpc_cidr_block = ec2.Vpc(subnet_vpc).cidr_block

    return vpc_cidr_block

@catch(ClientError, return_value='',
       msg='Unexpected error when getting internet gateway: {ex}')
@aws_client('ec2')
def get_igw_id(vpc_id, *, ec2=None):
    """Getting the virtual private gateway ID in the current account."""

    igw_response = ec2.describe_internet_gateways(Filters=dict_to_filters({
        'attachment.vpc-id': vpc_id,
    }))

    return igw_response['InternetGateways'][0]['InternetGatewayId']

@catch(ClientError, return_value='',
       msg='Unexpected error when getting VPC endpoint ID: {ex}')
@aws_client('ec2')
def get_vpce_id(vpc_id, service, *, ec2=None):
    '''Getting vpc S3 endpoint ID'''

    vpce_response = ec2.describe_vpc_endpoints(Filters=dict_to_filters({
        'vpc-id': vpc_id,
    }))
    for vpce in vpce_response['VpcEndpoints']:
        if service in vpce['ServiceName']:
            return vpce['VpcEndpointId']


@catch(ClientError, IndexError, reraise=True,
       msg='Unexpected error when getting route table and subnet information: {ex}')
def check_tgw_public_subnet_route_table(vpc_id, route_table_name):
    route_table = get_vpc_route_table(vpc_id, route_table_name)

    # getting the first subnet id where the route table associates with
    subnet_id = route_table['Associations'][0]['SubnetId']

    # getting vpc cidr, gateway id, vpc s3 endpoint id
    vpc_cidr = get_vpc_cidr(subnet_id)
    
    igw_id = get_igw_id(vpc_id)

    vpce_s3_id = get_vpce_id(vpc_id, 's3')


    route_count = 0
    local_route_found = False
    default_route_found = False
    s3_endpoint_route_found = False

    for aroute in route_table['Routes']:
        route_count += 1
        if 'DestinationCidrBlock' in aroute:
            print('find  cidr')
            if aroute['DestinationCidrBlock'] == vpc_cidr and aroute['GatewayId'] == 'local':
                local_route_found = True

            if aroute['DestinationCidrBlock'] == '0.0.0.0/0' and aroute['GatewayId'] == igw_id:
                default_route_found = True
            
        else:
            if aroute['GatewayId'] == vpce_s3_id:
                s3_endpoint_route_found = True

    # there must be exactly 3 routes. They are local, default, s3 end point, and 5 Emory routes
    assert route_count == 3
    assert local_route_found, 'Local route not found'
    assert default_route_found, 'Default route not found'
    assert s3_endpoint_route_found, 's3 endpoint route not found'

@pytest.mark.tgw_test
def test_tgw_mgmt_route_table_routes(vpc_id):
    check_tgw_public_subnet_route_table(vpc_id, ROUTE_TABLE_NAME)

