'''
----------------------------------
test_mgmt_subnet2_az.py
----------------------------------
"Type": "structural",
"Name": "test_mgmt_subnet2_az",
"Description": "Check to make sure the subnet is in the correct availability zone.",
"Plan": "Verify the 'MGMT2 - IT ONLY' subnet's availability zone is in the correct region and
         its availability zone is different from the 'MGMT1 - LITS ONLY' subnet's availability zone.
"ExpectedResult": "Success"

'''

from aws_test_functions import check_subnet_az


def test_answer(vpc_id):
    assert check_subnet_az(vpc_id, 'MGMT2 - IT ONLY', 'MGMT1 - IT ONLY', 'us-east-1')
