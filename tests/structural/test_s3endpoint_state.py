'''
------------------------
test_s3endpoint_state.py
------------------------

"Type": "structural",
"Name": "test_s3endpoint_state",
"Description": "Verify the vpc endpoint is available.",
"Plan": "Describe the vpc endpoint (need vpc-id and service-name) and verify the state is 'Available'.",
"ExpectedResult": "Success"

Loop through all of the VPCs in an account, describe each vpc endpoint, and verify the state of each is 'available'.

Returns true if the state is 'available'.
'''

from aws_test_functions import aws_client, dict_to_filters


@aws_client('ec2')
def check_s3endpoint_state(vpc_id, *, ec2=None):
    endpoint = ec2.describe_vpc_endpoints(Filters=dict_to_filters({
        'vpc-id': vpc_id,
        'service-name': 'com.amazonaws.us-east-1.s3',
    }))

    return endpoint['VpcEndpoints'][0]['State'] == 'available'


def test_answer(vpc_id):
    assert check_s3endpoint_state(vpc_id)
