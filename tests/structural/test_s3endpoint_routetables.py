'''
----------------------------------
test_s3endpoint_routetables.py
----------------------------------

"Type": "structural",
"Name": "test_s3endpoint_routetables",
"Description": "Verify the vpc endpoint is associated with correct route tables.",
"Plan": "Describe the vpc endpoint (need vpc-id and service-name) and verify there are three route tables associated with the endpoint.",
"ExpectedResult": "Success"

Verify the s3 endpoint contains three RouteTableIds.  Then check
to make sure each of those Route Tables has a route for a VPC
endpoint (by matching to 'vpce-')

Returns true if endpoint contains 3 RouteTableIds and each of those
route tables has a route for a VPC endpoint.
'''
import pytest
from aws_test_functions import aws_client, dict_to_filters


@aws_client('ec2')
def check_s3endpoint_routetables(vpc_id, vpn_connection_method, *, ec2=None):
    endpoint = ec2.describe_vpc_endpoints(Filters=dict_to_filters({
        'vpc-id': vpc_id,
        'service-name': 'com.amazonaws.us-east-1.s3',
    }))
    routeTableIds = endpoint['VpcEndpoints'][0]['RouteTableIds']

    if vpn_connection_method == 'VPN':
        if len(routeTableIds) != 3:
            return False

        endpoint_id = endpoint['VpcEndpoints'][0]['VpcEndpointId']

        vpceRouteMatchIndex = 0
        for routeTableId in routeTableIds:
            routetables = ec2.describe_route_tables(RouteTableIds=[routeTableId])
            routes = routetables['RouteTables'][0]['Routes']

            for route in routes:
                if route.get('GatewayId', None) is not None:
                    if route['GatewayId'] == endpoint_id:
                        vpceRouteMatchIndex += 1

        if vpceRouteMatchIndex != 3:
            return False
    else:
        if len(routeTableIds) != 4:
            return False

        endpoint_id = endpoint['VpcEndpoints'][0]['VpcEndpointId']

        vpceRouteMatchIndex = 0
        for routeTableId in routeTableIds:
            routetables = ec2.describe_route_tables(RouteTableIds=[routeTableId])
            routes = routetables['RouteTables'][0]['Routes']

            for route in routes:
                if route.get('GatewayId', None) is not None:
                    if route['GatewayId'] == endpoint_id:
                        vpceRouteMatchIndex += 1

        if vpceRouteMatchIndex != 4:
            return False

    return True

@pytest.mark.vpn_test
def test_answer(vpc_id):
    assert check_s3endpoint_routetables(vpc_id, 'VPN')

@pytest.mark.tgw_test
def test_tgw_s3endpoint_routetables(vpc_id):
    assert check_s3endpoint_routetables(vpc_id, 'TGW')
