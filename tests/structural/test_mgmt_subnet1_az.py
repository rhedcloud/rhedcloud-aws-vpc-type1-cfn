'''
----------------------------------
test_mgmt_subnet1_az.py
----------------------------------
"Type": "structural",
"Name": "test_mgmt_subnet1_az",
"Description": "Check to make sure the subnet is in the correct availability zone.",
"Plan": "Verify the 'MGMT1 - IT ONLY' subnet's availability zone is in the correct region and
         its availability zone is different from the 'MGMT2 - LITS ONLY' subnet's availability zone.
"ExpectedResult": "Success"

'''

from aws_test_functions import check_subnet_az


def test_answer(vpc_id):
    assert check_subnet_az(vpc_id, 'MGMT1 - IT ONLY', 'MGMT2 - IT ONLY', 'us-east-1')
