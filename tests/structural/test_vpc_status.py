'''
------------------
test_vpc_status.py
------------------

"Type": "structural",
"Name": "test_vpc_status",
"Description": "Verify that the VPC exists and has a status of available.",
"Plan": "Describe the VPC (need VPC ID) and check to see if the status is available.",
"ExpectedResult": "Success"

Verify all the Type1 VPCs in the account have a status of available by
describing each VPC and checking to see if the status is available.
Returns True if status is available.
'''

import boto3


def test_answer(vpc_id):
    ec2 = boto3.client('ec2')
    response = ec2.describe_vpcs(VpcIds=[vpc_id])
    assert response['Vpcs'][0]['State'] == 'available'
