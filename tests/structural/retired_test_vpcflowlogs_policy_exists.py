'''
-----------------
test_vpcflowlogs_policy_exists.py
-----------------

"Type": "structural",
"Name": "test_vpcflowlogs_policy_exists",
"Description": "Verify that the RHEDcloudVpcFlowLogsPolicy exists.",
"Plan": "Look up the policy name by ARN and compare to the expected result, RHEDcloudVpcFlowLogsPolicy.",
"ExpectedResult": "Success"

Verify that the VpcFlowLogsPolicy exists
in the account by
Looking up the policy name with ARN and compare to the expected result, RHEDcloudVpcFlowLogsPolicy.

'''

from aws_test_functions import (
    find_policy_for_stack,
    get_stack_name_from_vpc_id,
)


def test_policy_exists(vpc_id):
    stack_name = get_stack_name_from_vpc_id(vpc_id)
    arn = find_policy_for_stack(stack_name, 'RHEDcloudVpcFlowLogsPolicy')

    assert arn != 'RHEDcloudVpcFlowLogsPolicy'
    assert stack_name in arn
