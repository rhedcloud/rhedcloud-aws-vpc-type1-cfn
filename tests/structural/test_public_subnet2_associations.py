'''
-----------------------------------
test_public_subnet2_associations.py
-----------------------------------

"Type": "structural",
"Name": "test_public_subnet2_associations",
"Description": "Check to make sure the subnet is associated with the route table.",
"Plan": "Describe the route table (need route-table-id) and validate the subnet (need subnet-id) is associated.",
"ExpectedResult": "Success"

'''
import pytest
from aws_test_functions import check_subnet_association

@pytest.mark.vpn_test
def test_answer(vpc_id):
    assert check_subnet_association(vpc_id, 'Public Route Table', 'Public Subnet 2')

@pytest.mark.tgw_test
def test_public2_subnet_assoc(vpc_id):
    assert check_subnet_association(vpc_id, 'Public2RouteTable', 'Public Subnet 2')
