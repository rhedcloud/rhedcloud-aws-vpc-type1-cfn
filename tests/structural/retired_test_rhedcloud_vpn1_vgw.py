'''
----------------------------
test_rhedcloud_vpn1_cgw.py
----------------------------

"Type": "structural",
"Name": "test_rhedcloud_vpn1_vgw",
"Description": "Verify the vgw in the VPN setup matches the vgw for the VPC.",
"Plan": "Describe the VPN connection connecting to AWS Research VPC VPN Endpoint 1, by using tags, and check vgw.",
"ExpectedResult": "Success"

'''
import pytest
from aws_test_functions import aws_client, dict_to_filters


@aws_client('ec2')
def find_available_vgw_state(vpc_id, ec2=None):

    for vpn_conn_dict in ec2.describe_vpn_connections(Filters=dict_to_filters({
        'tag:Name': 'RHEDcloudVpnConnection1',
    }))['VpnConnections']:
        x = vpn_conn_dict['VpnGatewayId']
        vpn_vgw_dict = ec2.describe_vpn_gateways(VpnGatewayIds=[x])['VpnGateways'][0]
        for vgw in vpn_vgw_dict['VpcAttachments']:
            if vgw['VpcId'] != vpc_id:
                continue
            if vpn_conn_dict['State'] != 'available' or vgw['State'] != 'attached':
                return False
    return True

@pytest.mark.vpn_test
def test_answer(vpc_id):
    assert find_available_vgw_state(vpc_id)
