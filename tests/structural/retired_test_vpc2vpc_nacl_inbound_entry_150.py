'''
----------------
test_vpc2vpc_nacl_inbound_entry_150.py
----------------

"Type": "structural",
"Name": "test_vpc2vpc_nacl_inbound_entry_150",
"Description": "Verify the NACL entry exists.",
"Plan": "Describe the Network ACL (need nacl-id) and verify there is an entry that matches: CidrBlock=0.0.0.0/0, RuleNumber=150, Protocol=-1, Egress=false, and RuleAction=allow.",
"ExpectedResult": "Success"

This test loops through all of the VPCs of a particular type (specified as a parameter that is passed in)
and checks to make sure NACL inbound entry rule number 150 is present.
'''

from aws_test_functions import check_vpc2vpc_nacl_entry


def test_answer(vpc_id):
    assert check_vpc2vpc_nacl_entry(vpc_id, {
        'CidrBlock': '0.0.0.0/0',
        'Egress': False,
        'Protocol': '-1',
        'RuleAction': 'allow',
        'RuleNumber': 150
    })
