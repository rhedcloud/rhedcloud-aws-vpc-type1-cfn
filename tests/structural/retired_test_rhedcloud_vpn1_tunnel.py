'''
----------------------------
test_rhedcloud_vpn1_tunnel.py
----------------------------

"Type": "structural",
"Name": "test_rhedcloud_vpn1_tunnel",
"Description": "Verify the tunnnel inside cidr in the VPN setup.",
"Plan": "Describe the VPN connection connecting to AWS Research VPC VPN Endpoint 1, by using tags, and check tunnnel inside cidr.",
"ExpectedResult": "Success"
'''
import pytest
import ipaddress
import xml.etree.ElementTree as ET

from aws_test_functions import aws_client, dict_to_filters


@aws_client('ec2')
def check_vpn_connection_tunnels(vpc_id, vpn_tag_name, tunnel_cidr_1, tunnel_cidr_2, ec2=None):
    vpn_cidr_network_1 = ipaddress.ip_network(tunnel_cidr_1)
    vpn_cidr_network_2 = ipaddress.ip_network(tunnel_cidr_2)
    tunnel_1_exists = False
    tunnel_2_exists = False
    vpn_gateway_filter = dict_to_filters({
        'attachment.vpc-id': vpc_id,
    })
    for dict_vpn_gateway in ec2.describe_vpn_gateways(Filters=vpn_gateway_filter)['VpnGateways']:
        vpn_conn_filter = dict_to_filters({
            'tag:Name': vpn_tag_name,
            'vpn-gateway-id': dict_vpn_gateway['VpnGatewayId'],
            'state': 'available',
        })
        for vpn_conn_dict in ec2.describe_vpn_connections(Filters=vpn_conn_filter)['VpnConnections']:
            rootXml = ET.fromstring(vpn_conn_dict['CustomerGatewayConfiguration'])
            for ipsec_tunnel in rootXml.findall('ipsec_tunnel'):
                for vpn_gateway in ipsec_tunnel.findall('vpn_gateway'):
                    for tunnel_inside_address in vpn_gateway.findall('tunnel_inside_address'):
                        tunnel_1_exists = True
                        ip_address = tunnel_inside_address.find('ip_address').text
                        ip_from_describe = ipaddress.ip_network(ip_address)
                        if(not ip_from_describe.overlaps(vpn_cidr_network_1) and not ip_from_describe.overlaps(vpn_cidr_network_2)):
                            return False
            for ipsec_tunnel in rootXml.findall('ipsec_tunnel'):
                for customer_gateway in ipsec_tunnel.findall('customer_gateway'):
                    for tunnel_inside_address in customer_gateway.findall('tunnel_inside_address'):
                        tunnel_2_exists = True
                        ip_address = tunnel_inside_address.find('ip_address').text
                        ip_from_describe = ipaddress.ip_network(ip_address)
                        if(not ip_from_describe.overlaps(vpn_cidr_network_1) and not ip_from_describe.overlaps(vpn_cidr_network_2)):
                            return False

    # If the tunnels exist and none of the ip addresses matched the CIDR
    # ranges, the function would have already returned already
    if tunnel_1_exists and tunnel_2_exists:
        return True
    return False

@pytest.mark.vpn_test
def test_answer(vpc_id):
    assert check_vpn_connection_tunnels(vpc_id, 'RHEDcloudVpnConnection1', '169.254.10.0/30', '169.254.251.248/30')
