"""
==================
vpc_endpoint_tests
==================

This is a generic set of tests for VPC endpoints that rely on a fixture named
``endpoint_name`` to be defined. This module should be imported by other test
modules which each define a specific VPC endpoint name.

* Verify the endpoint name
* Verify the endpoint state
* Verify the number of availability zones for the endpoint

"""

pytest_plugins = "tests.plugins.endpoint"


def test_name(endpoint, full_endpoint_name):
    """Verify the VPC endpoint name.

    :param dict endpoint:
        Information about a VPC endpoint.
    :param str full_endpoint_name:
        Expected name for the VPC endpoint.

    """

    assert endpoint["ServiceName"] == full_endpoint_name


def test_state(endpoint):
    """Verify the VPC endpoint state.

    :param dict endpoint:
        Information about a VPC endpoint.

    """

    assert endpoint["State"] == "available"


def test_zones(endpoint):
    """Verify the number of availability zones for the VPC endpoint.

    :param dict endpoint:
        Information about a VPC endpoint.

    """

    assert len(endpoint["NetworkInterfaceIds"]) == 2
