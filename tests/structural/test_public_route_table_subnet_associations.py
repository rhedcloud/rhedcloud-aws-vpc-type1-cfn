'''
----------------------------------------------
test_public_route_table_subnet_associations.py
----------------------------------------------

"Type": "structural",
"Name": "test_public_route_table_subnet_associations",
"Description": "Check to make sure the two subnets are associated with the route table.",
"Plan": "Describe the route table (need route-table-id) and validate the two subnets (need subnet-ids) are correct.",
"ExpectedResult": "Success"

Check to make sure two public subnets are associated with the public route table.
'''

import pytest
from aws_test_functions import (
    check_route_table_subnet_associations,
)

VPN_ROUTE_TABLE_NAME = 'Public Route Table'
TGW_PUBLIC1_ROUTE_TABLE_NAME = 'Public1RouteTable'
TGW_PUBLIC2_ROUTE_TABLE_NAME = 'Public2RouteTable'
SUBNET_1_NAME = 'Public Subnet 1'
SUBNET_2_NAME = 'Public Subnet 2'

@pytest.mark.vpn_test
def test_answer(vpc_id):
    check_route_table_subnet_associations(
        vpc_id,
        VPN_ROUTE_TABLE_NAME,
        SUBNET_1_NAME,
        SUBNET_2_NAME,
    )

@pytest.mark.tgw_test
def test_public1_route_table_subnet_assoc(vpc_id):
    check_route_table_subnet_associations(
        vpc_id,
        TGW_PUBLIC1_ROUTE_TABLE_NAME,
        SUBNET_1_NAME,
    )

@pytest.mark.tgw_test
def test_public2_route_table_subnet_assoc(vpc_id):
    check_route_table_subnet_associations(
        vpc_id,
        TGW_PUBLIC2_ROUTE_TABLE_NAME,
        SUBNET_2_NAME,
    )
