'''
--------------------------------------------
test_mgmt_route_table_subnet_associations.py
--------------------------------------------

"Type": "structural",
"Name": "test_mgmt_route_table_subnet_associations",
"Description": "Check to make sure the two subnets are associated with the route table.",
"Plan": "Describe the route table (need route-table-id) and validate the two subnets (need subnet-ids) are correct.",
"ExpectedResult": "Success"

Check to make sure two mgmt ubnets are associated with the mgmt route table.
'''

from aws_test_functions import (
    check_route_table_subnet_associations,
)

ROUTE_TABLE_NAME = 'Management Route Table'
SUBNET_1_NAME = 'MGMT1 - IT ONLY'
SUBNET_2_NAME = 'MGMT2 - IT ONLY'


def test_answer(vpc_id):
    check_route_table_subnet_associations(
        vpc_id,
        ROUTE_TABLE_NAME,
        SUBNET_1_NAME,
        SUBNET_2_NAME,
    )
