'''
-------------------------
test_mgmt_subnet2_name.py
-------------------------
"Type": "structural",
"Name": "test_mgmt_subnet2_name",
"Description": "Verify the subnet is named correctly.",
"Plan": "Verify there is a subnet named 'MGMT2 - IT ONLY' in each RHEDcloud Type1 VPC
         in the account.,
"ExpectedResult": "Success"

'''

from aws_test_functions import check_subnet_name


def test_answer(vpc_id):
    assert check_subnet_name(vpc_id, 'MGMT2 - IT ONLY')
