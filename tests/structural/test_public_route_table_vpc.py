'''
------------------------------
test_public_route_table_vpc.py
------------------------------

"Type": "structural",
"Name": "test_public_route_table_vpc",
"Description": "Verify the route table is associated with the correct VPC.",
"Plan": "Describe the route table (need route-table-id) and check to see if the VPC matches the VpcId created by this template.",
"ExpectedResult": "Success"

Verify the route table is associated with the correct VPC.
'''
import pytest
from aws_test_functions import get_vpc_route_table

VPN_ROUTE_TABLE_NAME = 'Public Route Table'
TGW_PUBLIC1_ROUTE_TABLE_NAME = 'Public1RouteTable'
TGW_PUBLIC2_ROUTE_TABLE_NAME = 'Public2RouteTable'

@pytest.mark.vpn_test
def test_answer(vpc_id):
    assert get_vpc_route_table(vpc_id, VPN_ROUTE_TABLE_NAME)

@pytest.mark.tgw_test
def test_tgw_public1_route_table(vpc_id):
    assert get_vpc_route_table(vpc_id, TGW_PUBLIC1_ROUTE_TABLE_NAME)

@pytest.mark.tgw_test
def test_tgw_public2_route_table(vpc_id):
    assert get_vpc_route_table(vpc_id, TGW_PUBLIC2_ROUTE_TABLE_NAME)
