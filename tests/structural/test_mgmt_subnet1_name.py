'''
-------------------------
test_mgmt_subnet1_name.py
-------------------------
"Type": "structural",
"Name": "test_mgmt_subnet1_name",
"Description": "Verify the subnet is named correctly.",
"Plan": "Verify there is a subnet named 'MGMT1 - IT ONLY' in each RHEDcloud Type1 VPC
         in the account.,
"ExpectedResult": "Success"


'''

from aws_test_functions import check_subnet_name


def test_answer(vpc_id):
    assert check_subnet_name(vpc_id, 'MGMT1 - IT ONLY')
