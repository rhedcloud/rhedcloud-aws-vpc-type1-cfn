'''
-----------------
test_tgw_state.py
-----------------

"Type": "structural",
"Name": "test_tgw_attachment_state",
"Description": "Verify the TGW Attachment State is available",
"Plan": "Describe the TGW attachment and check that state is available.",
"ExpectedResult": "Success"

'''

import pytest
from aws_test_functions import aws_client, dict_to_filters

@aws_client('ec2')
def find_tgw_attachment_state(vpc_id, *, ec2=None):
    """Getting the virtual private gateway ID in the current account."""

    tgw_response = ec2.describe_transit_gateway_vpc_attachments(Filters=dict_to_filters({
        'vpc-id': vpc_id,
    }))

    if tgw_response['TransitGatewayVpcAttachments'][0]['State'] == 'available':
        return True
    else:
        return False

@pytest.mark.tgw_test
def test_answer(vpc_id):
    assert find_tgw_attachment_state(vpc_id)
