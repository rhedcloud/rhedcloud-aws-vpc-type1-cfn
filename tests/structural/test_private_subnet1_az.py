'''
----------------------------------
test_private_subnet1_az.py
----------------------------------
"Type": "structural",
"Name": "test_private_subnet1_az",
"Description": "Check to make sure the subnet is in the correct availability zone.",
"Plan": "Verify the 'Private Subnet 1' subnet's availability zone is in the correct region and
         its availability zone is different from the 'Private Subnet 2' subnet's availability zone.
"ExpectedResult": "Success"

'''

from aws_test_functions import check_subnet_az


def test_answer(vpc_id):
    assert check_subnet_az(vpc_id, 'Private Subnet 1', 'Private Subnet 2', 'us-east-1')
