'''
-----------------
test_rhedcloud_cgw1_state.py
-----------------

"Type": "structural",
"Name": "test_rhedcloud_cgw1_state",
"Description": "Verify the cgw state is available by checking its status.",
"Plan": "Describe the customer gateway connecting to AWS Research VPC VPN Endpoint 1, by using tags, and check state.",
"ExpectedResult": "Success"


'''
import pytest
from aws_test_functions import aws_client, dict_to_filters


@aws_client('ec2')
def find_available_rhedcloud_cgw_state(cgw_tag_name, ec2=None):
    """Function to describe CGW state. If the state is 'available' then its successful."""

    cgw_dict = ec2.describe_customer_gateways(Filters=dict_to_filters({
        'tag:Name': cgw_tag_name,
    }))
    ''' Result should be false if the CGW tag is not found '''

    accepted_states = {'available', 'deleted'}

    if not cgw_dict['CustomerGateways']:
        return False
    for cgw in cgw_dict['CustomerGateways']:
        if cgw['State'] not in accepted_states:
            return False
    return True

@pytest.mark.vpn_test
def test_answer(vpc_id):
    assert find_available_rhedcloud_cgw_state('RHEDcloud1CustomerGateway')
