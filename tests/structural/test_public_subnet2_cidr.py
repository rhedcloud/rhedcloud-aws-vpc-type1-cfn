'''
---------------------------
test_public_subnet2_cidr.py
---------------------------
"Type": "structural",
"Name": "test_public_subnet2_cidr",
"Description": "Check to make sure the CIDR space assigned for the subnet is correct.",
"Plan": "Verify the 'Public Subnet 2' subnets assigned to each RHEDcloud Type1 or Type2 VPC
         in the account is part of the primary CIDR block assigned to the VPC.",
"ExpectedResult": "Success"

'''

from aws_test_functions import check_subnet_cidr


def test_answer(vpc_id):
    assert check_subnet_cidr(vpc_id, 'Public Subnet 2')
