'''
----------------
test_vpc2vpc_nacl_associations.py
----------------

"Type": "structural",
"Name": "test_vpc2vpc_nacl_associations",
"Description": "Verify the six subnets created in this template (ManagementSubnet1, ManagementSubnet2, PublicSubnet1, PublicSubnet2, PrivateSubnet1, PrivateSubnet2) are associated with this NACL.",
"Plan": "Describe the Network ACL (need nacl-id) and verify the associated subnets (need subnet-ids) are correct.",
"ExpectedResult": "Success"

This test loops through all of the VPCs of a particular type (specified as a parameter that is passed in)
and checks to make sure the correct subnets are associated with the NACL.
'''

from aws_test_functions import (
    get_subnet_name,
    get_vpc_nacl_associations,
)


def test_answer(vpc_id):
    expected_names = {
        'MGMT1 - IT ONLY', 'MGMT2 - IT ONLY',
        'Public Subnet 1', 'Public Subnet 2',
        'Private Subnet 1', 'Private Subnet 2',
    }

    found = {
        get_subnet_name(association['SubnetId'])
        for association in get_vpc_nacl_associations(vpc_id)
    }

    if expected_names != found:
        print('Error: The following subnet(s) in {} were not associated to the NACL:'.format(vpc_id))
        for subnet_name in (found ^ expected_names):
            print(subnet_name)
        assert False

    assert True
