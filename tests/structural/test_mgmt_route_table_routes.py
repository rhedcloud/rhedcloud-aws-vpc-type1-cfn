'''
-------------------------------
test_mgmt_route_table_routes.py
-------------------------------

"Type": "structural",
"Name": "test_mgmt_route_table_routes",
"Description": "Verify there are three active routes in the route table and check to make sure they are the right ones.",
"Plan": "Describe the route table (need subnet ID) and verify the routes are correct (local - desitination CIDR block matches VPC CIDR block & GatewayId=local, default - desination CIDR Block is 0.0.0.0/0 & GatewayId matches VGW, and S3 endpoint - desitnation prefix begins with 'pl-') and active.",
"ExpectedResult": "Success"

Since the routes for TGW-attached VPCs use a TransitGatewayId rather than the VGW GatewayId, the expected results differ from VPN connected VPCs.
The check_subnet_route_table was altered to accept a new parameter and Pytest markers are used to indicate when each test should be run.

Verify there are three actice routes in the route table and check to make sure they are the right ones.
Describe the route table (need subnet ID) and verify the routes are correct
local - desitination CIDR block matches VPC CIDR block & GatewayId=local,
default - desination CIDR Block is 0.0.0.0/0 & GatewayId matches VGW or TransitGatewayID matches TGW,
S3 endpoint - desitnation prefix begins with 'pl-') and active.
'''
import pytest
from tests.structural.test_public_route_table_routes import check_subnet_route_table

ROUTE_TABLE_NAME = 'Management Route Table'


@pytest.mark.vpn_test
def test_vpn_mgmt_route_table_routes(vpc_id):
    check_subnet_route_table(vpc_id, ROUTE_TABLE_NAME, 'VPN')
