'''
-----------------------------------
test_public_subnet1_associations.py
-----------------------------------

"Type": "structural",
"Name": "test_public_subnet1_associations",
"Description": "Check to make sure the subnet is associated with the route table.",
"Plan": "Describe the route table (need route-table-id) and validate the subnet (need subnet-id) is associated.",
"ExpectedResult": "Success"

'''
import pytest
from aws_test_functions import check_subnet_association

@pytest.mark.vpn_test
def test_answer(vpc_id):
    assert check_subnet_association(vpc_id, 'Public Route Table', 'Public Subnet 1')

@pytest.mark.tgw_test
def test_public1_subnet_assoc(vpc_id):
    assert check_subnet_association(vpc_id, 'Public1RouteTable', 'Public Subnet 1')

