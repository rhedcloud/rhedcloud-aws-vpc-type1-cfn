"""
============================
test_cloudformation_endpoint
============================

"Type": "structural",
"Name": "test_cloudformation_endpoint",
"Description": "Verify the service name, state, and network interfaces.",
"Plan": "Service name should equal com.amazonaws.[REGION].cloudtrail; state should be available; and there should be two NetworkInterfaceIds",
"ExpectedResult": "Success"

"""

import pytest

from tests.structural.vpc_endpoint_tests import *


@pytest.fixture(scope='module')
def endpoint_name():
    return "cloudformation"
