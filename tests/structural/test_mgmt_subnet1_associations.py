'''
-----------------------------------
test_mgmt_subnet1_associations.py
-----------------------------------

"Type": "structural",
"Name": "test_mgmt_subnet1_associations",
"Description": "Check to make sure the subnet is associated with the route table.",
"Plan": "Describe the route table (need route-table-id) and validate the subnet (need subnet-id) is associated.",
"ExpectedResult": "Success"

'''

from aws_test_functions import check_subnet_association


def test_answer(vpc_id):
    assert check_subnet_association(vpc_id, 'Management Route Table', 'MGMT1 - IT ONLY')
