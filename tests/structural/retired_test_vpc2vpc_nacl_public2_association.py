'''
----------------
test_vpc2vpc_nacl_public2_association.py
----------------

"Type": "structural",
"Name": "test_vpc2vpc_nacl_public2_association",
"Description": "Verify the PublicSubnet2 are associated with this NACL.",
"Plan": "Describe the Network ACL (need nacl-id) and verify the associated subnet (need subnet-id) is present.",
"ExpectedResult": "Success"

This test loops through all of the VPCs of a particular type (specified as a parameter that is passed in)
and checks to make sure Public Subnet 2 subnet is associated with the network ACL.
'''

from aws_test_functions import check_subnet_nacl_association


def test_answer(vpc_id):
    assert check_subnet_nacl_association(vpc_id, 'Public Subnet 2')
