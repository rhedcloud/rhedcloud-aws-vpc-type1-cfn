"""
============================
test_mgmt_subnet_policy_info
============================

These tests verify that the RHEDcloudManagementSubnetPolicy was created as
expected.

Plan:

* verify that the policy exists
* verify the policy path
* verify the roles that are attached to the policy

${testcount:3}

"""

import boto3
import pytest

from aws_test_functions import find_policy_for_stack, get_stack_name_from_vpc_id


@pytest.fixture(scope="module")
def stack_name(vpc_id):
    return get_stack_name_from_vpc_id(vpc_id)


@pytest.fixture(scope="module")
def policy(stack_name):
    arn = find_policy_for_stack(stack_name, 'RHEDcloudManagementSubnetPolicy')
    return boto3.resource("iam").Policy(arn)


def test_exists(policy, stack_name):
    """Verify that the policy's name is as expected.

    :param IAM.Policy policy:
        The Policy to check.

    """

    assert policy.policy_name == "{}-RHEDcloudManagementSubnetPolicy".format(stack_name)


def test_policy_path(policy):
    """Verify the policy's path.

    :param IAM.Policy policy:
        The Policy to check.

    """

    assert policy.path == "/rhedcloud/"


def test_attached_roles(policy):
    """Verify that the policy has certain roles attached to it.

    :param IAM.Policy policy:
        The Policy to check.

    """

    roles = [r.name for r in policy.attached_roles.all()]
    assert len(roles) == 1
    assert "RHEDcloudAdministratorRole" in roles


def test_attached_groups(policy):
    """Verify that the policy has certain groups attached to it.

    :param IAM.Policy policy:
        The Policy to check.

    """

    groups = [g.name for g in policy.attached_groups.all()]
    assert len(groups) == 1
    assert "RHEDcloudServiceAccountGroup" in groups
