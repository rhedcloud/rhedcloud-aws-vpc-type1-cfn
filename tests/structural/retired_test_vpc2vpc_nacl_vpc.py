'''
----------------
test_vpc2vpc_nacl_vpc.py
----------------

"Type": "structural",
"Name": "test_vpc2vpc_nacl_vpc",
"Description": "Verify the right NACL is attached to the right VPC.",
"Plan": "Describe the Network ACLs for each Type1 VPC in an account and make sure there is a NACL with the name: VPC-to-VPC NACL.",
"ExpectedResult": "Success"

This test loops through all of the VPCs of a particular type (specified as a parameter that is passed in)
and checks to make sure there is one NACL named VPC-to-VPC NACL in each VPC.
'''

from aws_test_functions import get_vpc_nacl


def test_answer(vpc_id):
    assert get_vpc_nacl(vpc_id), 'NACL with the name VPC-to-VPC NACL not found in VPC {}'.format(vpc_id)
