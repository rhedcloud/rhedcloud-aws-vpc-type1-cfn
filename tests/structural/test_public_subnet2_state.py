'''
----------------------------
test_public_subnet2_state.py
----------------------------
"Type": "structural",
"Name": "test_public_subnet2_state",
"Description": "Verify the subnet is available by checking its status.",
"Plan": "Verify the 'Public Subnet 2' subnets assigned to each RHEDcloud Type1 or Type2 VPC
         in the account has a state of available.",
"ExpectedResult": "Success"
'''

from aws_test_functions import check_subnet_state


def test_answer(vpc_id):
    assert check_subnet_state(vpc_id, 'Public Subnet 2')
