'''
----------------------------
test_vgw_attachment_state.py
----------------------------

"Type": "structural",
"Name": "test_vgw_attachment_state",
"Description": "Verify the Virtual Gateway is attached",
"Plan": "Describe the vpn gateways and check that state is attached.",
"ExpectedResult": "Success"
-JKL

Added looping logic to account for multiple VPCs in an account.  -Paul

'''
import pytest
from aws_test_functions import aws_client


@aws_client('ec2')
def get_vgw_attachment_state(vpc_id, *, ec2=None):
    vgw_dict = ec2.describe_vpn_gateways()
    for vgw in vgw_dict['VpnGateways']:
        for attachment in vgw['VpcAttachments']:
            if vpc_id == attachment['VpcId'] and attachment['State'] != 'attached':
                return False

    return True

@pytest.mark.vpn_test
def test_answer(vpc_id):
    assert get_vgw_attachment_state('All')
