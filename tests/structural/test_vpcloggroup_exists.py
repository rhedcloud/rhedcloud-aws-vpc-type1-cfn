'''
----------------
test_vpcloggroup_exists.py
----------------

"Type": "structural",
"Name": "test_vpcloggroup_exists",
"Description": "Verify the VPC log group exists.",
"Plan": "Describe flow log for a VPC and verify LogGroupName equals vpc-id-FlowLogs.",
"ExpectedResult": "Success"

This test loops through all of the VPCs of a particular type (specified as a parameter that is passed in)
and checks to make sure log group exists by querying for the log name and making sure it matches
the expected naming convention for each VPC.
'''

from aws_test_functions import get_flow_log_attr


def test_answer(vpc_id):
    group_name = get_flow_log_attr(vpc_id, 'LogGroupName')
    expected = '{}-FlowLogs'.format(vpc_id)
    assert group_name == expected, 'Log group name does not exist or does not match expected naming convention for {}'.format(vpc_id)
