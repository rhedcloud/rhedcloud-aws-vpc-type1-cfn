'''
----------------
test_vpc2vpc_nacl_entries.py
----------------

"Type": "structural",
"Name": "test_vpc2vpc_nacl_entries",
"Description": "Check to make sure the NACL entries deny inbound access from the 10.64.0.0/16 CIDR (RuleNumber 100/egress=false), allow all other inbound access (RuleNumber 150/egress=false), and allow all oubound access (RuleNumber 100/egress=true).",
"Plan": "Describe the Network ACL (need nacl-id) and validate the three entries are correct.",
"ExpectedResult": "Success"

This test loops through all of the VPCs of a particular type (specified as a parameter that is passed in)
and checks to make sure the NACL Entries match the expected Entries.
'''

from aws_test_functions import (
    get_vpc_cidr,
    get_vpc_nacl_entries,
)


def test_answer(vpc_id):
    expected_nacl_entries = [
        {'CidrBlock': '0.0.0.0/0', 'Egress': True, 'Protocol': '-1', 'RuleAction': 'allow', 'RuleNumber': 100},
        {'CidrBlock': '0.0.0.0/0', 'Egress': True, 'Protocol': '-1', 'RuleAction': 'deny', 'RuleNumber': 32767},
        {'CidrBlock': get_vpc_cidr(vpc_id), 'Egress': False, 'Protocol': '-1', 'RuleAction': 'allow', 'RuleNumber': 50},
        {'CidrBlock': '10.64.0.0/16', 'Egress': False, 'Protocol': '-1', 'RuleAction': 'deny', 'RuleNumber': 100},
        {'CidrBlock': '0.0.0.0/0', 'Egress': False, 'Protocol': '-1', 'RuleAction': 'allow', 'RuleNumber': 150},
        {'CidrBlock': '0.0.0.0/0', 'Egress': False, 'Protocol': '-1', 'RuleAction': 'deny', 'RuleNumber': 32767},
    ]

    entries = get_vpc_nacl_entries(vpc_id)
    return entries == expected_nacl_entries, 'NACL entries do not match expected results for VPC {}'.format(vpc_id)
