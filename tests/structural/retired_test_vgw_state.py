'''
-----------------
test_vgw_state.py
-----------------

"Type": "structural",
"Name": "test_vgw_state",
"Description": "Verify the Virtual Gateway State is available",
"Plan": "Describe the virtual gateway and check that state is available.",
"ExpectedResult": "Success"

'''

from aws_test_functions import aws_client


@aws_client('ec2')
def find_available_vgw_state(vpc_id, *, ec2=None):
    """Function to describe VGW state. If the state is 'available' then its successful."""

    vgw_dict = ec2.describe_vpn_gateways()
    for vgw in vgw_dict['VpnGateways']:
        for attachment in vgw['VpcAttachments']:
            if vpc_id == attachment['VpcId'] and vgw['State'] != 'available':
                return False

    return True


def test_answer(vpc_id):
    assert find_available_vgw_state(vpc_id)
