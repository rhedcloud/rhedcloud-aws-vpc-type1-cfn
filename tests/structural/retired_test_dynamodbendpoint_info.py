"""
==========================
test_dynamodbendpoint_info
==========================

Verify that the DynamoDB VPC endpoint has been created as expected.

Plan:

* Verify the endpoint state
* Verify the number of route tables
* Verify the policy document

${testcount:9}

"""

import json

import pytest


pytest_plugins = "tests.plugins.endpoint"


@pytest.fixture(scope="module")
def endpoint_name():
    return "dynamodb"


def test_state(endpoint):
    """Verify the VPC endpoint state.

    :param dict endpoint:
        Information about a VPC endpoint.

    """

    assert endpoint["State"] == "available"


def test_routetables(endpoint):
    """Verify the number of route tables for the VPC endpoint.

    :param dict endpoint:
        Information about a VPC endpoint.

    """

    assert len(endpoint["RouteTableIds"]) == 3


def test_policy_document(endpoint):
    """Verify the policy document for the VPC endpoint.

    :param dict endpoint:
        Information about a VPC endpoint.

    """

    assert json.loads(endpoint["PolicyDocument"]) == {
        "Version": "2012-10-17",
        "Statement": [{
            "Action": "*",
            "Effect": "Allow",
            "Resource": "*",
            "Principal": "*"
        }],
    }
