'''
------------------------------
test_tgw_public_route_table_routes
------------------------------

"Type": "structural",
"Name": "test_tgw_public_route_table_routes",
"Description": "Verify there are eight active routes in the route table and check to make sure they are the right ones.",
"Plan": "Describe the route table (need subnet ID) and verify the routes are correct.",
"ExpectedResult": "Success"

Since the routes for TGW-attached VPCs use a TransitGatewayId rather than the VGW GatewayId, the expected results differ from VPN connected VPCs.

Verify there are eight active routes in the route table and check to make sure they are the right ones.
Describe the route table (need subnet ID) and verify the routes are correct
local - desitination CIDR block matches VPC CIDR block & GatewayId=local,
default - desination CIDR Block is 0.0.0.0/0 & GatewayId matches GWLB endpoint,
emory_routes - destination CIDR Block matches Emory CIDRs & GatewayId matches TGW ID,
S3 endpoint - desitnation prefix begins with 'pl-') and active.
'''

import pytest
from aws_test_functions import (
    ClientError,
    catch,
    aws_client,
    aws_resource,
    dict_to_filters,
    get_vpc_route_table,
)

PUBLIC1_ROUTE_TABLE_NAME = 'Public1RouteTable'
PUBLIC2_ROUTE_TABLE_NAME = 'Public2RouteTable'

@catch(ClientError, return_value='',
       msg='Unexpected error when getting VPC Cidr Block: {ex}')
@aws_resource('ec2')
def get_vpc_cidr(subnet_id, *, ec2=None):
    '''
    Getting the CIDR range associated with the VPC
    '''

    subnet_vpc = ec2.Subnet(subnet_id).vpc_id

    print('vpc id' + subnet_vpc)
    vpc_cidr_block = ec2.Vpc(subnet_vpc).cidr_block

    return vpc_cidr_block

@catch(ClientError, return_value='',
       msg='Unexpected error when getting transit gateway: {ex}')
@aws_client('ec2')
def get_tgw_id(vpc_id, *, ec2=None):
    """Getting the virtual private gateway ID in the current account."""

    tgw_response = ec2.describe_transit_gateway_vpc_attachments(Filters=dict_to_filters({
        'vpc-id': vpc_id,
    }))

    return tgw_response['TransitGatewayVpcAttachments'][0]['TransitGatewayId']

@catch(ClientError, return_value='',
       msg='Unexpected error when getting internet gateway: {ex}')
@aws_client('ec2')
def get_igw_id(vpc_id, *, ec2=None):
    """Getting the virtual private gateway ID in the current account."""

    igw_response = ec2.describe_internet_gateways(Filters=dict_to_filters({
        'vpc-id': vpc_id,
    }))

    return igw_response['InternetGateways'][0]['InternetGatewayId']

@catch(ClientError, return_value='',
       msg='Unexpected error when getting VPC endpoint ID: {ex}')
@aws_client('ec2')
def get_vpce_id(vpc_id, service, *, ec2=None):
    '''Getting vpc S3 endpoint ID'''

    vpce_response = ec2.describe_vpc_endpoints(Filters=dict_to_filters({
        'vpc-id': vpc_id,
    }))
    for vpce in vpce_response['VpcEndpoints']:
        if service in vpce['ServiceName']:
            return vpce['VpcEndpointId']

@catch(ClientError, return_value='',
       msg='Unexpected error when getting VPC endpoint ID: {ex}')
@aws_client('ec2')
def get_gwlb_vpce_id(vpc_id, ec2=None):
    '''Getting GWLB VPC endpoint IDs'''

    gwlb_endpoints = []
    gwlb_vpce_response = ec2.describe_vpc_endpoints(Filters=dict_to_filters({
        'vpc-id': vpc_id,
        'vpc-endpoint-type': 'GatewayLoadBalancer',
    }))
    for vpce in gwlb_vpce_response['VpcEndpoints']:
        gwlb_endpoints.append(vpce['VpcEndpointId'])

    return gwlb_endpoints


@catch(ClientError, IndexError, reraise=True,
       msg='Unexpected error when getting route table and subnet information: {ex}')
def check_tgw_public_subnet_route_table(vpc_id, route_table_name):
    route_table = get_vpc_route_table(vpc_id, route_table_name)

    # getting the first subnet id where the route table associates with
    subnet_id = route_table['Associations'][0]['SubnetId']

    # getting vpc cidr, gateway id, vpc s3 endpoint id
    vpc_cidr = get_vpc_cidr(subnet_id)
    

    transit_gateway_id = get_tgw_id(vpc_id)

    vpce_s3_id = get_vpce_id(vpc_id, 's3')
    vpce_gwlb_ids = get_gwlb_vpce_id(vpc_id)

    route_count = 0
    local_route_found = False
    emory_10_0_0_0_route_found = False
    emory_170_140_1_1_route_found = False		# DNS
    emory_170_140_2_1_route_found = False		# DNS
    emory_170_140_26_171_route_found = False	# AD
    emory_170_140_26_174_route_found = False	# AD
    emory_170_140_26_175_route_found = False	# AD
    emory_170_140_26_176_route_found = False	# AD
    emory_170_140_31_169_route_found = False	# Winship Printer
    emory_170_140_46_78_route_found = False		# SMTP
    emory_170_140_52_57_route_found = False		# Winship Web Server
    emory_170_140_52_58_route_found = False		# Internal Winship Server
    emory_170_140_53_96_route_found = False		# AD
    emory_170_140_53_97_route_found = False		# AD
    emory_170_140_106_20_route_found = False	# Splunk Heavy Forwarder
    emory_170_140_124_7_route_found = False		# F5 load-balancer
    emory_170_140_124_8_route_found = False		# F5 load-balancer
    emory_170_140_124_9_route_found = False		# F5 load-balancer
    emory_170_140_124_38_route_found = False    # news.web.emory.edu
    emory_170_140_125_5_route_found = False		# PPEGuide
    emory_170_140_125_23_route_found = False	# login.emory.edu
    emory_170_140_125_85_route_found = False	# F5 load-balancer (bogusname1)
    emory_170_140_125_115_route_found = False	# adfs
    emory_170_140_125_187_route_found = False	# LDS Auth
    emory_170_140_205_31_route_found = False	# AD
    emory_170_140_205_44_route_found = False	# AD
    emory_170_140_205_152_route_found = False	# AD
    emory_170_140_205_153_route_found = False	# AD    
    emory_172_16_0_0_route_found = False
    emory_192_168_0_0_route_found = False
    default_route_found = False
    s3_endpoint_route_found = False

    for aroute in route_table['Routes']:
        route_count += 1
        if 'DestinationCidrBlock' in aroute:
            print('find  cidr')
            if aroute['DestinationCidrBlock'] == vpc_cidr and aroute['GatewayId'] == 'local':
                local_route_found = True

            if aroute['DestinationCidrBlock'] == '10.0.0.0/8' and aroute['TransitGatewayId'] == transit_gateway_id:
                emory_10_0_0_0_route_found = True

            if aroute['DestinationCidrBlock'] == '170.140.1.1/32' and aroute['TransitGatewayId'] == transit_gateway_id:
                emory_170_140_1_1_route_found = True

            if aroute['DestinationCidrBlock'] == '170.140.2.1/32' and aroute['TransitGatewayId'] == transit_gateway_id:
                emory_170_140_2_1_route_found = True
 
            if aroute['DestinationCidrBlock'] == '170.140.26.171/32' and aroute['TransitGatewayId'] == transit_gateway_id:
                 emory_170_140_26_171_route_found = True

            if aroute['DestinationCidrBlock'] == '170.140.26.174/32' and aroute['TransitGatewayId'] == transit_gateway_id:
                 emory_170_140_26_174_route_found = True
            
            if aroute['DestinationCidrBlock'] == '170.140.26.175/32' and aroute['TransitGatewayId'] == transit_gateway_id:
                 emory_170_140_26_175_route_found = True

            if aroute['DestinationCidrBlock'] == '170.140.26.176/32' and aroute['TransitGatewayId'] == transit_gateway_id:
                 emory_170_140_26_176_route_found = True
            
            if aroute['DestinationCidrBlock'] == '170.140.31.169/32' and aroute['TransitGatewayId'] == transit_gateway_id:
                 emory_170_140_31_169_route_found = True

            if aroute['DestinationCidrBlock'] == '170.140.46.78/32' and aroute['TransitGatewayId'] == transit_gateway_id:
                 emory_170_140_46_78_route_found = True

            if aroute['DestinationCidrBlock'] == '170.140.52.57/32' and aroute['TransitGatewayId'] == transit_gateway_id:
                 emory_170_140_52_57_route_found = True

            if aroute['DestinationCidrBlock'] == '170.140.52.58/32' and aroute['TransitGatewayId'] == transit_gateway_id:
                 emory_170_140_52_58_route_found = True

            if aroute['DestinationCidrBlock'] == '170.140.53.96/32' and aroute['TransitGatewayId'] == transit_gateway_id:
                 emory_170_140_53_96_route_found = True

            if aroute['DestinationCidrBlock'] == '170.140.53.97/32' and aroute['TransitGatewayId'] == transit_gateway_id:
                 emory_170_140_53_97_route_found = True
                
            if aroute['DestinationCidrBlock'] == '170.140.106.20/32' and aroute['TransitGatewayId'] == transit_gateway_id:
                 emory_170_140_106_20_route_found = True

            if aroute['DestinationCidrBlock'] == '170.140.124.7/32' and aroute['TransitGatewayId'] == transit_gateway_id:
                 emory_170_140_124_7_route_found = True

            if aroute['DestinationCidrBlock'] == '170.140.124.8/32' and aroute['TransitGatewayId'] == transit_gateway_id:
                 emory_170_140_124_8_route_found = True

            if aroute['DestinationCidrBlock'] == '170.140.124.9/32' and aroute['TransitGatewayId'] == transit_gateway_id:
                 emory_170_140_124_9_route_found = True

            if aroute['DestinationCidrBlock'] == '170.140.124.38/32' and aroute['TransitGatewayId'] == transit_gateway_id:
                 emory_170_140_124_38_route_found = True

            if aroute['DestinationCidrBlock'] == '170.140.125.5/32' and aroute['TransitGatewayId'] == transit_gateway_id:
                 emory_170_140_125_5_route_found = True
            
            if aroute['DestinationCidrBlock'] == '170.140.125.23/32' and aroute['TransitGatewayId'] == transit_gateway_id:
                 emory_170_140_125_23_route_found = True

            if aroute['DestinationCidrBlock'] == '170.140.125.85/32' and aroute['TransitGatewayId'] == transit_gateway_id:
                 emory_170_140_125_85_route_found = True

            if aroute['DestinationCidrBlock'] == '170.140.125.115/32' and aroute['TransitGatewayId'] == transit_gateway_id:
                 emory_170_140_125_115_route_found = True

            if aroute['DestinationCidrBlock'] == '170.140.125.187/32' and aroute['TransitGatewayId'] == transit_gateway_id:
                 emory_170_140_125_187_route_found = True

            if aroute['DestinationCidrBlock'] == '170.140.205.31/32' and aroute['TransitGatewayId'] == transit_gateway_id:
                 emory_170_140_205_31_route_found = True

            if aroute['DestinationCidrBlock'] == '170.140.205.44/32' and aroute['TransitGatewayId'] == transit_gateway_id:
                 emory_170_140_205_44_route_found = True

            if aroute['DestinationCidrBlock'] == '170.140.205.152/32' and aroute['TransitGatewayId'] == transit_gateway_id:
                 emory_170_140_205_152_route_found = True

            if aroute['DestinationCidrBlock'] == '170.140.205.153/32' and aroute['TransitGatewayId'] == transit_gateway_id:
                 emory_170_140_205_153_route_found = True

            if aroute['DestinationCidrBlock'] == '172.16.0.0/12' and aroute['TransitGatewayId'] == transit_gateway_id:
                emory_172_16_0_0_route_found = True

            if aroute['DestinationCidrBlock'] == '192.168.0.0/16' and aroute['TransitGatewayId'] == transit_gateway_id:
                emory_192_168_0_0_route_found = True

            if aroute['DestinationCidrBlock'] == '0.0.0.0/0' and aroute['GatewayId'] in vpce_gwlb_ids:
                default_route_found = True
            
        else:
            if aroute['GatewayId'] == vpce_s3_id:
                s3_endpoint_route_found = True

    # there must be exactly 32 routes. They are local, default, s3 end point, the 3 private Emory routes, and 26 host routes.
    assert route_count == 32
    assert local_route_found, 'Local route not found'
    assert emory_10_0_0_0_route_found, 'Emory 10.0.0.0/8 route not found'
    assert emory_170_140_1_1_route_found, 'Emory 170.140.1.1/32 route not found'
    assert emory_170_140_2_1_route_found, 'Emory 170.140.2.1/32 route not found'
    assert emory_170_140_26_171_route_found, 'Emory 170.140.26.171/32 route not found'
    assert emory_170_140_26_174_route_found, 'Emory 170.140.26.174/32 route not found'
    assert emory_170_140_26_175_route_found, 'Emory 170.140.26.175/32 route not found'
    assert emory_170_140_26_176_route_found, 'Emory 170.140.26.176/32 route not found'
    assert emory_170_140_31_169_route_found, 'Emory 170.140.31.169/32 route not found'
    assert emory_170_140_46_78_route_found, 'Emory 170.140.46.78/32 route not found'
    assert emory_170_140_52_57_route_found, 'Emory 170.140.52.57/32 route not found'
    assert emory_170_140_52_58_route_found, 'Emory 170.140.52.58/32 route not found'
    assert emory_170_140_53_96_route_found, 'Emory 170.140.53.96/32 route not found'
    assert emory_170_140_53_97_route_found, 'Emory 170.140.53.97/32 route not found'
    assert emory_170_140_106_20_route_found, 'Emory 170.140.106.20/32 route not found'
    assert emory_170_140_124_7_route_found, 'Emory 170.140.124.7/32 route not found'
    assert emory_170_140_124_8_route_found, 'Emory 170.140.124.8/32 route not found'
    assert emory_170_140_124_9_route_found, 'Emory 170.140.124.9/32 route not found'
    assert emory_170_140_124_38_route_found, 'Emory 170.140.124.38/32 route not found'
    assert emory_170_140_125_5_route_found, 'Emory 170.140.125.5/32 route not found'
    assert emory_170_140_125_23_route_found, 'Emory 170.140.125.23/32 route not found'
    assert emory_170_140_125_85_route_found, 'Emory 170.140.125.85/32 route not found'
    assert emory_170_140_125_115_route_found, 'Emory 170.140.125.85/32 route not found'
    assert emory_170_140_125_187_route_found, 'Emory 170.140.125.187/32 route not found'
    assert emory_170_140_205_31_route_found, 'Emory 170.140.205.31/32 route not found'
    assert emory_170_140_205_44_route_found, 'Emory 170.140.205.44/32 route not found'
    assert emory_170_140_205_152_route_found, 'Emory 170.140.205.152/32 route not found'
    assert emory_170_140_205_153_route_found, 'Emory 170.140.205.152/32 route not found'
    assert emory_172_16_0_0_route_found, 'Emory 172.16.0.0/12 route not found'
    assert emory_192_168_0_0_route_found, 'Emory 192.168.0.0/16 route not found'
    assert default_route_found, 'Default route not found'
    assert s3_endpoint_route_found, 's3 endpoint route not found'

@pytest.mark.tgw_test
def test_tgw_public1_route_table_routes(vpc_id):
    check_tgw_public_subnet_route_table(vpc_id, PUBLIC1_ROUTE_TABLE_NAME)

@pytest.mark.tgw_test
def test_tgw_public2_route_table_routes(vpc_id):
    check_tgw_public_subnet_route_table(vpc_id, PUBLIC2_ROUTE_TABLE_NAME)