'''
----------------------------
test_rhedcloud_vpn1_cgw.py
----------------------------

"Type": "structural",
"Name": "test_rhedcloud_vpn1_cgw",
"Description": "Verify the cgw in the VPN setup matches the cgw for the AWS Research VPC VPN Endpoint 1.",
"Plan": "Describe the VPN connection connecting to AWS Research VPC VPN Endpoint 1, by using tags, and check cgw.",
"ExpectedResult": "Success"

'''
import pytest
from aws_test_functions import aws_client, dict_to_filters


@aws_client('ec2')
def check_vpn_connection_cgw(vpc_id, vpn_tag_name, cgw_tag_name, ec2=None):
    vpn_gateway_filter = dict_to_filters({
        'attachment.vpc-id': vpc_id,
    })
    for dict_vpn_gateway in ec2.describe_vpn_gateways(Filters=vpn_gateway_filter)['VpnGateways']:
        vpn_conn_filter = dict_to_filters({
            'tag:Name': vpn_tag_name,
            'vpn-gateway-id': dict_vpn_gateway['VpnGatewayId'],
        })
        for dict_vpn in ec2.describe_vpn_connections(Filters=vpn_conn_filter)['VpnConnections']:
            cgw_filter = dict_to_filters({
                'tag:Name': cgw_tag_name,
            })
            dict_cgw = ec2.describe_customer_gateways(Filters=cgw_filter, CustomerGatewayIds=[dict_vpn['CustomerGatewayId']])['CustomerGateways']
            if dict_cgw:
                return True
    return False

@pytest.mark.vpn_test
def test_answer(vpc_id):
    assert check_vpn_connection_cgw(vpc_id, 'RHEDcloudVpnConnection1', 'RHEDcloud1CustomerGateway')
