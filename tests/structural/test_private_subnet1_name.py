'''
----------------------------
test_private_subnet1_name.py
----------------------------
"Type": "structural",
"Name": "test_private_subnet1_name",
"Description": "Verify the subnet is named correctly.",
"Plan": Verify there is a subnet named 'Private Subnet 1' in each RHEDcloud Type1 VPC
        in the account.
"ExpectedResult": "Success"

'''

from aws_test_functions import check_subnet_name


def test_answer(vpc_id):
    assert check_subnet_name(vpc_id, 'Private Subnet 1')
