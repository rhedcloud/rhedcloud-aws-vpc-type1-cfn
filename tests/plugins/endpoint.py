import pytest

from aws_test_functions import dict_to_filters


@pytest.fixture(scope='module')
def full_endpoint_name(endpoint_name):
    return "com.amazonaws.us-east-1.{}".format(endpoint_name)


@pytest.fixture(scope='module')
def endpoint(session, vpc_id, full_endpoint_name):
    """Return information about a single VPC endpoint.

    :returns:
        A dictionary.

    """

    ec2 = session.client("ec2")
    res = ec2.describe_vpc_endpoints(
        Filters=dict_to_filters({
            "vpc-id": vpc_id,
            "service-name": full_endpoint_name,
        }),
    )

    return res["VpcEndpoints"][0]
