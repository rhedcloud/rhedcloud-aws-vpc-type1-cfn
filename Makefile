MARK ?= not incremental
KEYWORD ?= test
# ARGS ?= -n 8
ARGS ?=
# REPO ?= rhedcloud-aws-vpc-type1-cfn
# ARTIFACT ?= $(REPO).latest.zip
# BB_TEAM ?= rhedcloud

# download:
# 	download_artifact.sh $(REPO) $(ARTIFACT)
# 	cfn_tool.py compact $(REPO)/$(REPO).json
# 	mv $(REPO)/$(REPO).compact.json $(REPO).compact.json

test:
	pytest -m "$(MARK)" -k "$(KEYWORD)" $(ARGS)

slowtest:
	$(MAKE) MARK='slowtest' test

vpntest:
	$(MAKE) MARK='not tgw_test' test

tgwtest:
	$(MAKE) MARK='not vpn_test' test

# ----------------------------------------------------------------------------
# The following variables and targets are present to try to clean up and
# recreate the pipeline script in Bitbucket.
# ----------------------------------------------------------------------------

# These variables may be overridden by environment variables with the same name
CLOUDFORMATION_STACK_NAME ?= rhedcloud-aws-rs-account
CLOUDTRAIL_BUCKET_NAME ?= rhedcloud-pipeline-2-ct1
CLOUDTRAIL_NAME ?= $(CLOUDFORMATION_STACK_NAME)-Master
RHEDCLOUD_ACCOUNT_NAME ?= RHEDcloud Pipeline 2
REPO_NAME ?= rhedcloud-aws-vpc-type1-cfn
BB_TEAM ?= rhedcloud

CHECK_CMD ?= stack-state.py check $(REPO_NAME) \
	./rhedcloud-aws-rs-account-cfn/rhedcloud-aws-rs-account-cfn.json \
	./rhedcloud-aws-vpc-type1-cfn.json > /dev/null 2>&1

# Download the artifacts required for the tests in this repository
get-artifacts:
	download_artifact.sh rhedcloud-aws-rs-account-cfn rhedcloud-aws-rs-account-cfn.latest.zip

# Cache the checksums of each CloudFormation template used for this repository
# so we can determine whether to rebuild the stacks at a later time.
cache-states: get-artifacts
	stack-state.py save $(REPO_NAME) \
		./rhedcloud-aws-rs-account-cfn/rhedcloud-aws-rs-account-cfn.json \
		./rhedcloud-aws-vpc-type1-cfn.json

# Retrieve necessary artifacts and run a sequence of targets to clean and
# rebuild our stacks.
# rebuild: get-artifacts clean rs-account vpc-type1 cache-states script-runners
rebuild: get-artifacts clean rs-account vpc-type1 script-runners

# Retrieve necessary artifacts and run a sequence of targets to clean and
# rebuild our stacks.  Will build VPC with VPN parameter specified.
# rebuild-vpn: get-artifacts force-full-clean rs-account vpc-type1-vpn cache-states script-runners
rebuild-vpn: get-artifacts force-full-clean rs-account vpc-type1-vpn script-runners

# Retrieve necessary artifacts and run a sequence of targets to clean and
# rebuild our stacks.  Will build VPC with VPN parameter specified.
# rebuild-tgw: force-vpc-clean vpc-type1-tgw cache-states script-runners
rebuild-tgw: force-vpc-clean vpc-type1-tgw script-runners

# Run a sequence of targets to clean and rebuild our stacks.
local-rebuild: clean rs-account vpc-type1 script-runners

# Utility to determine whether a stack rebuild is necessary
should-rebuild:
	bash -c "$(CHECK_CMD) && echo No rebuild necessary || echo Rebuild necessary"

force-full-clean:
	$(MAKE) clean-runners clean-resources clean-bucket clean-stack

# Clean up S3 buckets, runners, and Type 1 VPC if stack changes are detected
force-vpc-clean:
	$(MAKE) clean-runners clean-resources clean-stack

# Clean up S3 buckets, runners, and Type 1 VPC if stack changes are detected
clean:
	bash -c "$(CHECK_CMD) && $(MAKE) clean-resources || $(MAKE) clean-runners clean-resources clean-bucket clean-stack"

# Remove resources left behind by previous tests
clean-resources:
	python ./bin/cleanup.py

# Remove the CloudTrail bucket
clean-bucket:
	s3_delete_bucket.py $(CLOUDTRAIL_BUCKET_NAME)

# Remove the test runner EC2 instances
clean-runners:
	rhedcloud_script_runners.py --no-move destroy || echo 'Continuing...'

# Remove the Type 1 VPC CloudFormation stacks in parallel
clean-stack:
	$(MAKE) type1-vpc-names | grep -v make | machma --replace PROFILE --timeout 20m -- cfn_stack_delete.py PROFILE

# Create the rs-account CloudFormation stack
rs-account:
	bash -c "$(CHECK_CMD) || cfn_stack_create.py \
		--parameter CloudTrailName=$(CLOUDTRAIL_BUCKET_NAME) \
		--compact $(CLOUDFORMATION_STACK_NAME) \
		./rhedcloud-aws-rs-account-cfn/rhedcloud-aws-rs-account-cfn.json"

# Create the Type 1 VPC CloudFormation stacks
vpc-type1:
	bash -c "$(CHECK_CMD) || $(MAKE) create-type1-vpcs"

# Create a Type 1 VPC CloudFormation Stack with a VpcCnnectionMethod equal to TGW.
vpc-type1-tgw:
	bash -c "cfn_stack_create.py \
		--parameter VpcConnectionMethod=TGW \
		--compact rhedcloud-aws-vpc-type1-0 \
		rhedcloud-aws-vpc-type1-cfn.json"

# Create a Type 1 VPC CloudFormation Stack with a VpcCnnectionMethod equal to VPN.
vpc-type1-vpn:
	bash -c "cfn_stack_create.py \
		--parameter VpcConnectionMethod=VPN \
		--compact rhedcloud-aws-vpc-type1-0 \
		rhedcloud-aws-vpc-type1-cfn.json"

# Print a list of Type 1 VPC names
type1-vpc-names:
	@find params/ -type f -iname '*.ini' | sed -E 's,.*/(.*)\.ini,\1,' | sort -u

# Create each of the desired Type 1 VPCs in parallel
create-type1-vpcs:
	$(MAKE) type1-vpc-names | grep -v make | machma --replace PROFILE --timeout 15m -- cfn_stack_create.py --parameters-file params/PROFILE.ini --compact PROFILE rhedcloud-aws-vpc-type1-cfn.json

# Create two new EC2 instances to run scripts
script-runners:
	rhedcloud_script_runners.py --no-move setup

# Upload test artifacts
upload:
	git archive --format zip --output $(REPO_NAME).latest.zip master
	cp $(REPO_NAME).json $(REPO_NAME).latest.json
	cp $(REPO_NAME).latest.zip $(REPO_NAME)-${BITBUCKET_BRANCH}-${BITBUCKET_BUILD_NUMBER}.zip
	upload_artifact.sh $(REPO_NAME).latest.json $(REPO_NAME).latest.zip
	upload_artifact_s3.sh $(REPO_NAME)-${BITBUCKET_BRANCH}-${BITBUCKET_BUILD_NUMBER}.zip emory-bitbucket-downloads /$(REPO_NAME)/

.EXPORT_ALL_VARIABLES:
