from contextlib import ExitStack

import pytest

from aws_test_functions import (
    attach_policy,
    cleanup,
    create_session_for_test_user,
    new_test_user,
)


pytest_plugins = [
    'aws_test_functions.plugin.aws_script_runner',
    'aws_test_functions.plugin.configurableskip',
]


@pytest.fixture(scope='session')
def user():
    """Create a default test user"""

    policies = (
        'rhedcloud/RHEDcloudAdministratorRolePolicy',
        'AdministratorAccess',
        'rhedcloud/rhedcloud-aws-vpc-type1-0-RHEDcloudManagementSubnetPolicy',
    )

    with ExitStack() as stack:
        u = stack.enter_context(new_test_user('test_standard_scp'))
        stack.enter_context(attach_policy(u, *policies))
        # stack.enter_context(in_test_org())

        yield u


@pytest.fixture(scope='session')
def session(user):
    """Create a default session for the test user"""

    return create_session_for_test_user(user.user_name)


@pytest.fixture(scope='session', autouse=True)
def cleanup_resources():
    """Automatically clean up unused resources before running tests."""

    cleanup.cleanup()


def pytest_runtest_makereport(item, call):
    if "incremental" in item.keywords:
        if call.excinfo is not None:
            parent = item.parent
            parent._prev_failed = item


def pytest_runtest_setup(item):
    if "incremental" in item.keywords:
        prev_failed = getattr(item.parent, '_prev_failed', None)
        if prev_failed is not None:
            pytest.xfail('previous test failed ({})'.format(prev_failed.name))
